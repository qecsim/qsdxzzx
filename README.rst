qsdxzzx
=======

**qsdxzzx** is a Python 3 package that extends `qecsim`_ with the XZZX surface
code.

.. _qecsim: https://github.com/qecsim/qecsim

The *qecsim* package is a quantum error correction simulator, which can be
extended with new codes, error models and decoders that integrate into its
command-line interface. This *qsdxzzx* package is a qecsim extension that
includes XZZX surface codes and decoders as described in the research article
*The XZZX Surface Code* (`arXiv:2009.07851`_).

.. _`arXiv:2009.07851`: https://arxiv.org/abs/2009.07851

|

User installation
-----------------

If you simply want to run qsdxzzx simulations then a user installation is
sufficient in most cases. (Some simulations require a developer installation,
see `<./data/README.rst>`__ for an explanation.) A user installation is achieved
by using the standard Python tool `pip`_ to install directly from the remote
qsdxzzx repository.

.. _pip: https://pip.pypa.io/en/stable/quickstart/

* Create a virtual environment (recommended, optional):

.. code-block:: text

    $ python3 --version                     # qsdxzzx requires Python 3.5+
    Python 3.7.8
    $ python3 -m venv venv                  # create virtual environment
    $ source venv/bin/activate              # activate venv (Windows: venv\Scripts\activate)
    (venv) $

* Install qsdxzzx with runtime dependencies:

.. code-block:: text

    (venv) $ pip install git+https://bitbucket.org/qecsim/qsdxzzx.git
    ...
    Successfully installed ... qsdxzzx-0.1b6

**Note**: Installation time depends on Internet connection speed but is
typically less than 3 minutes.

|

Run simulations
---------------

Simulations, such as those used to produce the data for the article
*The XZZX Surface Code*, are executed via the qecsim command line. A basic
example is provided here; see `<./data/README.rst>`__ for more details.

* Run 10 simulations with an XZZX planar code (distance = 13), depolarizing
  noise (error probability = 0.14), and tensor-network-based approximate
  maximum-likelihood decoding (bond dimension = 16):

.. code-block:: text

    (venv) $ qecsim run -r10 "xzzx.rotated_planar(13)" "generic.depolarizing" "xzzx.rotated_planar.rmps(16)" 0.14
    ...
    [{"code": "Rotated planar XZ 13", "decoder": "Rotated planar XZ RMPS (chi=16, mode=c)",
    "error_model": "Depolarizing", "error_probability": 0.14, "logical_failure_rate": 0.1,
    ... "wall_time": 3.8895596619999995}]

* Same example via module script with Python optimize flag for faster runtimes:

.. code-block:: text

    (venv) $ python3 -O -m qecsim run -r10 "xzzx.rotated_planar(13)" "generic.depolarizing" "xzzx.rotated_planar.rmps(16)" 0.14
    ...

|

Simulation data
---------------

Details of the data produced for the article *The XZZX Surface Code*, along
with corresponding simulation commands and parameters, are provided in
`<./data/README.rst>`__.

|

Developer installation
----------------------

If you want to modify the code of qsdxzzx, run unit tests, or build
distributables and documentation then a developer installation is required.
A developer installation is achieved by cloning the qsdxzzx repository and
using pip to install from your cloned repository in editable mode.

* Install qsdxzzx in editable mode with developer dependencies:

.. code-block:: text

    $ python3 --version                     # qsdxzzx requires Python 3.5+
    Python 3.7.8
    $ git clone https://bitbucket.org/qecsim/qsdxzzx.git  # clone qsdxzzx repository
    $ cd qsdxzzx
    $ python3 -m venv venv                  # create virtual environment
    $ source venv/bin/activate              # activate venv (Windows: venv\Scripts\activate)
    (venv) $ pip install -U setuptools pip  # install / upgrade setuptools and pip
    ...
    Successfully installed pip-21.0.1 setuptools-53.0.0
    (venv) $ pip install -e .[dev]          # install qsdxzzx with dev tools
    ...
    Successfully installed ... qsdxzzx ...

* Test installation:

.. code-block:: text

    (venv) $ pytest                         # run tests
    ...
    ==== 265 passed in 10.49s =====

|

Developer tools
---------------

Tasks for running tests with coverage, checking style, generating documentation
and building source and binary distributables can be executed using tox_; see
`<./tox.ini>`__ for more details.

.. _tox: https://tox.readthedocs.io/

For example, documentation can be built as follows:

.. code-block:: text

    (venv) $ tox -edocs                     # build documentation
    ...
      docs: commands succeeded
      congratulations :)
    (venv) $ ls ./dist/                     # list documentation
    qsdxzzx-0.1b6-docs.zip

|

Validated environments
----------------------

qsdxzzx has no special hardware requirements. It has been tested with the
following software environments:

==================== ======================================================
Operating system     CentOS Linux 8
Python               3.7.4
qecsim               1.0b8
+qecsim dependencies click=7.1.2, mpmath=1.1.0, networkx=2.5, numpy=1.17.2,
                     scipy=1.3.1
==================== ======================================================

==================== ======================================================
Operating system     macOS Mojave 10.14.6
Python               3.7.8
qecsim               1.0b8
+qecsim dependencies click=7.1.2, mpmath=1.1.0, networkx=2.5, numpy=1.19.5,
                     scipy=1.6.0
==================== ======================================================

|

License
-------

qsdxzzx is released under the BSD 3-Clause license; see `<LICENSE>`__.

|

Citing
------

If you use qecsim in your research, please see the `qecsim documentation`_ for
citing details. If you use qsdxzzx in your research, please cite the following
article:

* J. P. Bonilla Ataides, D. K. Tuckett, S. D. Bartlett, S. T. Flammia, and
  B. J. Brown, The XZZX Surface Code, (2020), `arXiv:2009.07851`_ [quant-ph].

A suitable BibTeX entry is:

.. code-block:: text

    @article{xzzx2020,
      title={The {XZZX} Surface Code},
      author={J. Pablo \surname{Bonilla Ataides} and David K. Tuckett
        and Stephen D. Bartlett and Steven T. Flammia and Benjamin J. Brown},
      year={2020},
      eprint={2009.07851},
      archivePrefix={arXiv},
      primaryClass={quant-ph}
    }

.. _qecsim documentation: https://davidtuckett.com/qit/qecsim/

|

Links
-----

* Source code: https://bitbucket.org/qecsim/qsdxzzx/
* qecsim source code: https://github.com/qecsim/qecsim
* qecsim documentation: https://davidtuckett.com/qit/qecsim/
* Contact: qecsim@gmail.com
