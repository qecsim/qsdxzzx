The XZZX Surface Code: Source Data
==================================

Here we provide details of the data produced for the research article
*The XZZX Surface Code* (`arXiv:2009.07851`_), along with simulation commands.

.. _`arXiv:2009.07851`: https://arxiv.org/abs/2009.07851

After covering the prerequisities, we describe the data and simulation commands
for each section of the article in turn.

|

Prerequisites
-------------

In order to execute the simulation commands, the `qsdxzzx`_ package should be
installed. See the qsdxzzx `<../README.rst>`__ file for installation
instructions.

.. _qsdxzzx: https://bitbucket.org/qecsim/qsdxzzx/

Notes:

* Much of the software is written as `qecsim`_ extensions and integrates into
  the qecsim command line. For such simulations, i.e. those using the ``qecsim``
  command, a simple *User installation* is sufficient.

  .. _qecsim: https://github.com/qecsim/qecsim

* The software for the sections *Fault-Tolerant Thresholds* and
  *Sub-Threshold Scaling* is not written as qecsim extensions but does rely on
  some qecsim services. For such simulations, the source code is required and,
  therefore, a *Developer installation* is more appropriate.

* To improve the performance of simulations using minimum-weight
  perfect-matching decoders, you many also consider installing an optional fast
  matching library; see the *Installation* section of `qecsim documentation`_
  for details. *(If the fast matching library is not installed, warning messages
  "Failed to load clib: libpypm.so" may be displayed and a fallback Python
  matching library will be used.)*

  .. _qecsim documentation: https://davidtuckett.com/qit/qecsim/

|

Optimal Thresholds
------------------

In the *Optimal Thresholds* section of the article, simulations use both CSS
and XZZX planar codes with tensor-network-based approximate maximum-likelihood
decoding over a sample of all single-qubit Pauli error channels. The source code
used for these simulations is available in
`<../src/qsdxzzx/rotatedplanarxz/>`__.

In order to estimate a threshold for a given code, error model and decoder, a
number of simulations are run to output logical failure rates for a range of
code distances and error probabilities.

* Example 1: using the CSS planar code with depolarizing noise and approximate
  maximum-likelihood decoding, the following command executes 10 simulations
  for code distance 13 and error probability 0.14.

.. code-block:: text

    (venv) $ qecsim run -r10 "rotated_planar(13,13)" "generic.depolarizing" "rotated_planar.rmps(16)" 0.14
    ...
    [{"code": "Rotated planar 13x13", "custom_totals": null, "decoder": "Rotated planar RMPS (chi=16, mode=c)", "error_model": "Depolarizing", "error_probability": 0.14, "error_weight_pvar": 13.49, "error_weight_total": 241, "logical_failure_rate": 0.1, "measurement_error_probability": 0.0, "n_fail": 1, "n_k_d": [169, 1, 13], "n_logical_commutations": [0, 1], "n_run": 10, "n_success": 9, "physical_error_rate": 0.14260355029585797, "time_steps": 1, "wall_time": 3.9226133639999996}]

General single-qubit Pauli error channels are specified with respect to the
surface of all single-qubit Pauli error channels, which forms a triangle with
vertices (1, 0, 0), (0, 1, 0), and (0, 0, 1) in Euclidean x, y, z coordinates.
The error channel is defined by ``qecsim.models.generic.CenterSliceErrorModel``
with parameters *limit* and *position*, where limit is a point on the boundary
of the triangle and position defines a point on a linear scale along the line
from the center of the triangle at position 0, to the limit, at position 1. An
error model parameterized in this way corresponds to a specific ratio of Pauli
X:Y:Z error rates and hence a specific single-qubit Pauli error channel. See
the *API* section of `qecsim documentation`_ for further details.

* Example 2: using the XZZX planar code with the error model defined by
  ``CenterSliceErrorModel`` with limit (0.68,0.00,0.32) and position 0.64
  (corresponding to the ratio of Pauli X:Y:Z error rates 0.5552:0.12:0.3248)
  and approximate maximum-likelihood decoding, the following command executes
  10 simulations for code distance 13 and error probability 0.17

.. code-block:: text

    (venv) $ qecsim run -r10 "xzzx.rotated_planar(13)" "generic.center_slice((0.68,0.00,0.32),0.64)" "xzzx.rotated_planar.rmps(16)" 0.17
    ...
    [{"code": "Rotated planar XZ 13", "custom_totals": null, "decoder": "Rotated planar XZ RMPS (chi=16, mode=c)", "error_model": "Center-slice (lim=(0.68, 0.0, 0.32), pos=0.64)", "error_probability": 0.17, "error_weight_pvar": 45.6, "error_weight_total": 280, "logical_failure_rate": 0.1, "measurement_error_probability": 0.0, "n_fail": 1, "n_k_d": [169, 1, 13], "n_logical_commutations": [0, 1], "n_run": 10, "n_success": 9, "physical_error_rate": 0.16568047337278108, "time_steps": 1, "wall_time": 3.9000329040000006}]

The final figure in the *Optimal Thresholds* section of the article, shows that
near-optimal thresholds can be achieved with a non-rotated rectangular lattice
XZZX code using MWPM. To obtain logical failure rates for code-capacity
threshold estimation in this case, use the qecsim extensions defined in
`<../src/qsdxzzx/planarxz/>`__. This implementation uses a redefined error model
with the standard CSS code to simulate Z-biased noise on the XZZX code.

* Example 3: checking for Z-type logical failure on a non-rotated rectangular
  XZZX code subject to Z-biased noise, the following command executes 10
  simulations on a 15x75 code with bias coefficient 3 and error probability
  0.19. (To check for X-type logical failure, the error model parameter
  primal=False is replaced by True).

.. code-block:: text

    (venv) $ qecsim run -r10 "planar(15,75)" "xzzx.planar.biased_pd(3,False)" "xzzx.planar.biased_pd" 0.19
    ...
    [{"code": "Planar 15x75", "custom_totals": null, "decoder": "Planar biased-primal/dual (degeneracy=True)", "error_model": "Planar biased-primal/dual (bias=3, primal=False)", "error_probability": 0.19, "error_weight_pvar": 152.65, "error_weight_total": 2355, "logical_failure_rate": 0.0, "measurement_error_probability": 0.0, "n_fail": 0, "n_k_d": [2161, 1, 15], "n_logical_commutations": [0, 0], "n_run": 10, "n_success": 10, "physical_error_rate": 0.1089773253123554, "time_steps": 1, "wall_time": 14.686081979999999}]

The source data, and corresponding simulation parameters, used to produce the
figures in the *Optimal Thresholds* section of the article are given in the
files:

* `<./optimal_thresholds_xzzx-thr-v-pauli.csv>`__: Figure 2 (XZZX code).
* `<./optimal_thresholds_css-thr-v-pauli.csv>`__: Figure 2 (CSS code).
* `<./optimal_thresholds_xzzx-thr-v-bias.csv>`__: Figure 3(a) (XZZX code).
* `<./optimal_thresholds_css-thr-v-bias.csv>`__: Figure 3(a) (CSS code).
* `<./optimal_thresholds_xzzx-thr-v-distances.csv>`__: Figure 3(b).
* `<./optimal_thresholds_xzzx-conv-v-bias.csv>`__: Figure 8(b).
* `<./code_capacity_thresholds_rectangular_xzzx-thr-v-bias.csv>`__:
  Figure 4 (XZZX code).

|

Fault-Tolerant Thresholds
-------------------------

In the *Fault-Tolerant Thresholds* section of the article, simulations use the
XZZX code and a biased noise model toward dephasing. Decoding is achieved
through minimum-weight perfect-matching (MWPM).

To obtain logical failure rates for fault-tolerant threshold estimation for a
square lattice XZZX code using MWPM, use the module
`<../src/qsdxzzx/toricxz/ft_square_lattice_decoder.py>`__

* Example 1: using the XZZX square lattice code with an error model biased
  toward dephasing with bias coefficient of 10, physical error probability
  of 0.055 and measurement error probability of q = p_Z + p_X, as used in the
  article. 10 simulations are run with a square lattice of dimension 12.

.. code-block:: text

    (venv) $ python ft_square_lattice_decoder.py p=0.055 bias=10 L=12 N=10
    ...
    [{'bias: 10.0', 'n_run: 10', 'wall_time: 3.294013500213623', 'logical_failure_rate: 0.8', 'n_fail: 8', 'code: Rotated square XZ 12x12', 'error_probability: 0.055', 'measurement_error_probability: 0.052500000000000005', 'decoder: Rotated square XZ MWPM', 'error_model: Biased noise toward dephasing'}]

The source data, and corresponding simulation parameters, used to produce the
figures in the *Fault-Tolerant Thresholds* section of the article are given in
the files:

* `<./ft_thresholds_xzzx-thr-v-bias.csv>`__: Figure 5(e) (XZZX code).

|

Sub-Threshold Scaling
---------------------

In the *Sub-Threshold Scaling* section of the article, simulations use the
coprime Lx(L+1) lattice XZZX code and a biased noise model toward dephasing.
Decoding is achieved through MWPM in the quadratic scaling regime and the
Metropolis sampling algorithm and splitting method, described in the article
*Simulation of rare events in quantum error correction* (`arXiv:1308.6270`_), is
used for the results depicting the boosted linear scaling of the logical failure
rate at low physical failure rate.

.. _`arXiv:1308.6270`: https://arxiv.org/abs/1308.6270

To obtain the logical failure rate at moderate physical error rate for a coprime
Lx(L+1) XZZX code using MWPM use the module
`<../src/qsdxzzx/toricxz/coprime_lattice_mwpm_decoder.py>`__

* Example 1: using the XZZX coprime Lx(L+1) lattice code with an error model
  biased toward dephasing with bias coefficient of 300, physical error
  probability of 0.2 and measurement error probability of 0. 1000 simulations
  are run with a coprime Lx(L+1) lattice of dimension L=7.

.. code-block:: text

    (venv) $ python coprime_lattice_mwpm_decoder.py p=0.2 bias=300 L=7 N=1000
    ...
    [{'error_model: Biased noise toward dephasing', 'error_probability: 0.2', 'n_fail: 12', 'logical_failure_rate: 0.012', 'bias: 300.0', 'n_run: 1000', 'decoder: Rotated coprime Lx(L+1) XZ MWPM', 'measurement_error_probability: 0', 'code: Rotated coprime XZ 7x8', 'wall_time: 1.3095722198486328'}]

To obtain the logical failure rate at low physical error rate for a coprime
Lx(L+1) lattice XZZX code using the metropolis hastings algorithm and splitting
method of `arXiv:1308.6270`_ , use the module
`<../src/qsdxzzx/toricxz/coprime_low_p_sampler.py>`__

* Example 2: using the XZZX coprime Lx(L+1) lattice code with an error model
  biased toward dephasing with bias coefficient of 1. The program calculates the
  logical failure rate, P, at physical error rate p=1e-3 for a coprime Lx(L+1)
  lattice of dimension L=7 given that we know that at p=0.1, P=0.02359. N=5000
  metropolis steps are run and error configurations are sampled after N_cut=1000
  metropolis steps. The initialised error configuration is the logical string
  consisting of alternating Z and X operators in the first row of the lattice.

.. code-block:: text

    (venv) $ python coprime_low_p_sampler.py p=1e-3 bias=3 L=7 p_known=0.1 P_known=0.02359 N=5000 N_cut=1000
    ...
    [{'code: Rotated coprime XZ 7x8', 'bias: 3.0', 'wall_time: 42.880813121795654', 'logical_failure_rate: 4.968023402316316e-10', 'known_P: 0.02359', 'error_probability: 0.001', 'n_discarded: 0', 'decoder: Rotated coprime XZ MWPM', 'error_model: Biased noise toward dephasing', 'n_Metropolis_runs: 5000', 'known_p: 0.1'}]

The source data, and corresponding simulation parameters, used to produce the
figures in the *Sub-Threshold Scaling* section of the article  and accompanying
Method section *Ansatz at low error rates* are given in the files:

* `<./quadratic_scaling_main_plot-coprime-xzzx-code.csv>`__: Figure 6(a) (main).
* `<./quadratic_scaling_inset_plot-coprime-xzzx-code.csv>`__:
  Figure 6(a) (inset).
* `<./logical_rate_at_low_physical_rate_main_plot-coprime-xzzx-code.csv>`__:
  Figure 6(b) (markers).
* `<./logical_rate_at_low_physical_rate-P-v-dis-main.csv>`__:
  Figure 10(a) (main).
* `<./logical_rate_at_low_physical_rate-P-v-dis-inset.csv>`__:
  Figure 10(a) (inset).
* `<./logical_rate_at_low_physical_rate-intercept-of-grad-of-best-fit-lines.csv>`__:
  Figure 10(b).
