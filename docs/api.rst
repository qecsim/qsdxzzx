API
===

.. toctree::
    :maxdepth: 2

    api/planarxz
    api/rotatedplanarxz
    api/toricxz
