planarxz: planar XZ stabilizer codes
====================================

``qsdxzzx.planarxz``
--------------------
.. automodule:: qsdxzzx.planarxz

``qsdxzzx.planarxz.PlanarBiasedPDDecoder``
------------------------------------------
.. autoclass:: qsdxzzx.planarxz.PlanarBiasedPDDecoder
    :show-inheritance:
    :members:
    :inherited-members:
    :special-members: __init__

``qsdxzzx.planarxz.PlanarBiasedPDErrorModel``
---------------------------------------------
.. autoclass:: qsdxzzx.planarxz.PlanarBiasedPDErrorModel
    :show-inheritance:
    :members:
    :inherited-members:
    :special-members: __init__
