"""
This module contains tests of the qsdxzzx package.
"""
import qsdxzzx


def test_version():
    """Example test: tests that qsdxzzx.__version__ returns a string."""
    version = qsdxzzx.__version__
    print(version)
    assert isinstance(version, str)
