import pytest
from qecsim import paulitools as pt

from qsdxzzx.rotatedplanarxz import RotatedPlanarXZCode


# < Code tests >

@pytest.mark.parametrize('distance', [
    3, 5, 7,
])
def test_rotated_planar_xz_code_properties(distance):
    code = RotatedPlanarXZCode(distance)
    assert isinstance(code.label, str)
    assert isinstance(repr(code), str)


@pytest.mark.parametrize('distance', [
    0, 1, 2, 4, 6, None, 'asdf', 5.1
])
def test_rotated_planar_xz_code_new_invalid_parameters(distance):
    with pytest.raises((ValueError, TypeError), match=r"^RotatedPlanarXZCode") as exc_info:
        RotatedPlanarXZCode(distance)
    print(exc_info)


@pytest.mark.parametrize('distance, expected', [
    (3, (9, 1, 3)),
    (5, (25, 1, 5)),
    (7, (49, 1, 7)),
])
def test_rotated_planar_xz_code_n_k_d(distance, expected):
    code = RotatedPlanarXZCode(distance)
    assert code.n_k_d == expected


@pytest.mark.parametrize('distance, expected', [
    (3, 8),
    (5, 24),
    (7, 48),
])
def test_rotated_planar_xz_code_stabilizers(distance, expected):
    assert len(RotatedPlanarXZCode(distance).stabilizers) == expected


def test_rotated_planar_xz_code_logical_xs():
    assert len(RotatedPlanarXZCode(5).logical_xs) == 1


def test_rotated_planar_xz_code_logical_zs():
    assert len(RotatedPlanarXZCode(5).logical_zs) == 1


def test_rotated_planar_xz_code_logicals():
    assert len(RotatedPlanarXZCode(5).logicals) == 2


@pytest.mark.parametrize('distance', [
    3, 5, 7,
])
def test_rotated_planar_xz_code_validate(distance):
    code = RotatedPlanarXZCode(distance)
    code.validate()  # no error raised


# </ Code tests >
# < Lattice tests >


@pytest.mark.parametrize('distance', [
    3, 5, 7,
])
def test_rotated_planar_xz_lattice_size(distance):
    lattice = RotatedPlanarXZCode(distance)
    assert lattice.size == (distance, distance)


@pytest.mark.parametrize('lattice, expected', [
    # RotatedPlanarCode(distance), max_site_i
    (RotatedPlanarXZCode(3), (2, 2)),
    (RotatedPlanarXZCode(5), (4, 4)),
    (RotatedPlanarXZCode(7), (6, 6)),
])
def test_rotated_planar_xz_lattice_site_bounds(lattice, expected):
    assert lattice.site_bounds == expected


@pytest.mark.parametrize('index, expected', [
    ((0, 1), True),  # in-bounds left
    ((4, 1), True),  # in-bounds right
    ((2, 4), True),  # in-bounds top
    ((2, 0), True),  # in-bounds bottom
    ((-1, 1), False),  # out-bounds left
    ((5, 1), False),  # out-bounds right
    ((2, 5), False),  # out-bounds top
    ((2, -1), False),  # out-bounds bottom
])
def test_rotated_planar_xz_lattice_is_in_site_bounds(index, expected):
    lattice = RotatedPlanarXZCode(5)
    assert lattice.is_in_site_bounds(index) == expected


@pytest.mark.parametrize('index, expected', [
    # 5x5 around boundary clockwise from (-1, -1)
    ((-1, 0), True),
    ((-1, 1), False),
    ((-1, 2), True),
    ((-1, 3), False),
    ((0, 4), True),
    ((1, 4), False),
    ((2, 4), True),
    ((3, 4), False),
    ((4, 3), True),
    ((4, 2), False),
    ((4, 1), True),
    ((4, 0), False),
    ((3, -1), True),
    ((2, -1), False),
    ((1, -1), True),
    ((0, -1), False),
    # 5x5 inside corners
    ((0, 0), True),
    ((0, 3), True),
    ((3, 3), True),
    ((3, 0), True),
    # 5x5 outside corners
    ((-1, -1), False),
    ((-1, 4), False),
    ((4, 4), False),
    ((4, -1), False),
])
def test_rotated_planar_xz_lattice_is_in_plaquette_bounds(index, expected):
    lattice = RotatedPlanarXZCode(5)
    assert lattice.is_in_plaquette_bounds(index) == expected


@pytest.mark.parametrize('index, expected', [
    # corners
    ((-1, -1), True),  # virtual sw
    ((-1, 4), True),  # virtual nw
    ((4, 4), True),  # virtual ne
    ((4, -1), True),  # virtual se
    # boundary virtual
    ((-1, 1), True),  # virtual w
    ((4, 0), True),  # virtual e
    ((1, 4), True),  # virtual n
    ((2, -1), True),  # virtual s
    # boundary real
    ((-1, 0), False),  # real w
    ((4, 1), False),  # real e
    ((2, 4), False),  # real n
    ((3, -1), False),  # real s
    # beyond boundary
    ((-2, 1), False),  # out-bounds w
    ((5, 1), False),  # out-bounds e
    ((2, 5), False),  # out-bounds n
    ((2, -2), False),  # out-bounds s
    # bulk
    ((0, 0), False),  # bulk sw
    ((0, 1), False),  # bulk nw
    ((3, 1), False),  # bulk ne
    ((3, 0), False),  # bulk se
])
def test_rotated_planar_xz_lattice_is_virtual_plaquette(index, expected):
    lattice = RotatedPlanarXZCode(5)
    assert lattice.is_virtual_plaquette(index) == expected


@pytest.mark.parametrize('error, expected', [
    # X
    (RotatedPlanarXZCode(5).new_pauli().site('X', (2, 2)), [(1, 1), (2, 2)]),  # center
    (RotatedPlanarXZCode(5).new_pauli().site('X', (0, 0)), [(0, 0)]),  # SW
    (RotatedPlanarXZCode(5).new_pauli().site('X', (0, 2)), [(0, 2)]),  # W
    (RotatedPlanarXZCode(5).new_pauli().site('X', (0, 4)), [(0, 4)]),  # NW
    (RotatedPlanarXZCode(5).new_pauli().site('X', (2, 4)), [(1, 3), (2, 4)]),  # N
    (RotatedPlanarXZCode(5).new_pauli().site('X', (4, 4)), [(3, 3)]),  # NE
    (RotatedPlanarXZCode(5).new_pauli().site('X', (4, 2)), [(3, 1)]),  # E
    (RotatedPlanarXZCode(5).new_pauli().site('X', (4, 0)), [(3, -1)]),  # SE
    (RotatedPlanarXZCode(5).new_pauli().site('X', (2, 0)), [(1, -1), (2, 0)]),  # S
    # Z
    (RotatedPlanarXZCode(5).new_pauli().site('Z', (2, 2)), [(1, 2), (2, 1)]),  # center
    (RotatedPlanarXZCode(5).new_pauli().site('Z', (0, 0)), [(-1, 0)]),  # SW
    (RotatedPlanarXZCode(5).new_pauli().site('Z', (0, 2)), [(-1, 2), (0, 1)]),  # W
    (RotatedPlanarXZCode(5).new_pauli().site('Z', (0, 4)), [(0, 3)]),  # NW
    (RotatedPlanarXZCode(5).new_pauli().site('Z', (2, 4)), [(2, 3)]),  # N
    (RotatedPlanarXZCode(5).new_pauli().site('Z', (4, 4)), [(4, 3)]),  # NE
    (RotatedPlanarXZCode(5).new_pauli().site('Z', (4, 2)), [(3, 2), (4, 1)]),  # E
    (RotatedPlanarXZCode(5).new_pauli().site('Z', (4, 0)), [(3, 0)]),  # SE
    (RotatedPlanarXZCode(5).new_pauli().site('Z', (2, 0)), [(1, 0)]),  # S
    # Y
    (RotatedPlanarXZCode(5).new_pauli().site('Y', (2, 2)), [(1, 1), (2, 2), (1, 2), (2, 1)]),  # center
    (RotatedPlanarXZCode(5).new_pauli().site('Y', (0, 0)), [(0, 0), (-1, 0)]),  # SW
    (RotatedPlanarXZCode(5).new_pauli().site('Y', (0, 2)), [(0, 2), (-1, 2), (0, 1)]),  # W
    (RotatedPlanarXZCode(5).new_pauli().site('Y', (0, 4)), [(0, 4), (0, 3)]),  # NW
    (RotatedPlanarXZCode(5).new_pauli().site('Y', (2, 4)), [(1, 3), (2, 4), (2, 3)]),  # N
    (RotatedPlanarXZCode(5).new_pauli().site('Y', (4, 4)), [(3, 3), (4, 3)]),  # NE
    (RotatedPlanarXZCode(5).new_pauli().site('Y', (4, 2)), [(3, 1), (3, 2), (4, 1)]),  # E
    (RotatedPlanarXZCode(5).new_pauli().site('Y', (4, 0)), [(3, -1), (3, 0)]),  # SE
    (RotatedPlanarXZCode(5).new_pauli().site('Y', (2, 0)), [(1, -1), (2, 0), (1, 0)]),  # S
])
def test_rotated_planar_xz_lattice_syndrome_to_plaquette_indices(error, expected):
    code = error.code
    syndrome = pt.bsp(error.to_bsf(), code.stabilizers.T)
    assert set(code.syndrome_to_plaquette_indices(syndrome)) == set(expected)


@pytest.mark.parametrize('pauli', [
    RotatedPlanarXZCode(3).new_pauli().site('Y', (0, 0), (1, 1)),
    RotatedPlanarXZCode(5).new_pauli().site('Y', (0, 0), (1, 1)),
    RotatedPlanarXZCode(3).new_pauli().logical_x(),
    RotatedPlanarXZCode(3).new_pauli().logical_z(),
    RotatedPlanarXZCode(5).new_pauli().logical_x(),
    RotatedPlanarXZCode(5).new_pauli().logical_z(),
])
def test_rotated_planar_xz_lattice_ascii_art(pauli):
    code = pauli.code
    syndrome = pt.bsp(pauli.to_bsf(), code.stabilizers.T)
    assert isinstance(code.ascii_art(), str)
    print(code.ascii_art())
    assert isinstance(code.ascii_art(syndrome=syndrome), str)
    print(code.ascii_art(syndrome=syndrome))
    assert isinstance(code.ascii_art(pauli=pauli), str)
    print(code.ascii_art(pauli=pauli))
    assert isinstance(code.ascii_art(syndrome=syndrome, pauli=pauli), str)
    print(code.ascii_art(syndrome=syndrome, pauli=pauli))


@pytest.mark.parametrize('code, plaquette_labels, site_labels', [
    (RotatedPlanarXZCode(3), {(-1, -1): '0', (0, 0): '1', (1, 2): '2'}, {(0, 0): 'a', (2, 1): 'b'}),
    (RotatedPlanarXZCode(5), {(0, 3): '0', (1, 1): '1', (3, 2): '2'}, {(0, 3): 'a', (3, 0): 'b'}),
])
def test_rotated_planar_xz_lattice_ascii_art_labels(code, plaquette_labels, site_labels):
    ascii_art = code.ascii_art(plaquette_labels=plaquette_labels, site_labels=site_labels)
    print()
    print(ascii_art)
    assert isinstance(ascii_art, str)

# </ Lattice tests >
