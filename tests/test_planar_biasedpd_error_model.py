import numpy as np
import pytest
from qecsim.models.planar import PlanarCode

from qsdxzzx.planarxz import PlanarBiasedPDErrorModel


@pytest.mark.parametrize('bias, primal', [
    (0.5, True),
    (10, False),
])
def test_planar_biasedpd_error_model_properties(bias, primal):
    em = PlanarBiasedPDErrorModel(bias, primal)
    assert em.bias == bias
    assert em.primal == primal
    assert isinstance(em.label, str)
    assert isinstance(repr(em), str)


@pytest.mark.parametrize('bias, primal', [
    (-1, True),
    ('sd', False),
])
def test_planar_biasedpd_error_model_new_invalid_parameters(bias, primal):
    with pytest.raises((ValueError, TypeError), match=r"^PlanarBiasedPDErrorModel") as exc_info:
        PlanarBiasedPDErrorModel(bias, primal)
    print(exc_info)


@pytest.mark.parametrize('bias', [
    0.5, 1, 3, 10, 30, 100, 300, 1000,
])
def test_planar_biasedpd_error_model_prob_dist(bias):
    # floating point tolerances (better than default)
    rtol, atol = 0.0, 1e-15
    em = PlanarBiasedPDErrorModel(bias, True)
    p = 0.1
    p_i, p_x, p_y, p_z = em.probability_distribution(p)
    assert np.isclose(p_i, 1 - p, rtol=rtol, atol=atol), 'p_I != 1 - p.'
    assert np.isclose(p_x + p_y + p_z, p, rtol=rtol, atol=atol), 'p_X + p_Y + p_Z != p'
    assert p_x == p_y, 'p_X !- p_Y'
    assert np.isclose(p_z, bias * (p_x + p_y), rtol=rtol, atol=atol), 'p_Z != bias * p_X + p_Y'
    assert em.p_high_rate(p) == p_y + p_z, 'p_hr != p_Y + p_Z'
    assert em.p_low_rate(p) == p_x + p_y, 'p_lr != p_X + p_Y'


@pytest.mark.parametrize('primal', [
    True, False
])
def test_planar_biasedpd_error_model_edge_probabilities(primal):
    # parameters
    code = PlanarCode(200, 200)
    bias = 10
    p = 0.3
    em = PlanarBiasedPDErrorModel(bias, primal)
    # generate error
    error = em.generate(code, p, np.random.default_rng(5))
    error_pauli = code.new_pauli(error)
    # count errors
    h_edge_count = 0
    v_edge_count = 0
    h_edge_error_count = 0
    v_edge_error_count = 0
    max_r, max_c = code.bounds
    for r in range(0, max_r + 1):
        for c in range(0, max_c + 1):
            index = r, c
            if code.is_site(index):
                if code.is_primal(index):  # horizontal
                    h_edge_count += 1
                    if error_pauli.operator(index) != 'I':
                        h_edge_error_count += 1
                else:  # vertical
                    v_edge_count += 1
                    if error_pauli.operator(index) != 'I':
                        v_edge_error_count += 1
    # checks
    if em.primal:
        p_hr_actual = v_edge_error_count / v_edge_count
        p_lr_actual = h_edge_error_count / h_edge_count
    else:
        p_hr_actual = h_edge_error_count / h_edge_count
        p_lr_actual = v_edge_error_count / v_edge_count
    p_hr_expected = em.p_high_rate(p)
    p_lr_expected = em.p_low_rate(p)
    print(p_hr_actual, p_hr_expected)
    print(p_lr_actual, p_lr_expected)
    # floating point tolerances
    rtol, atol = 1e-2, 0  # i.e. correct to 2 s.f.
    assert np.isclose(p_hr_actual, p_hr_expected, rtol=rtol, atol=atol)
    assert np.isclose(p_lr_actual, p_lr_expected, rtol=rtol, atol=atol)
