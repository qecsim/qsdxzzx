import numpy as np
import pytest
from qecsim import paulitools as pt, app
from qecsim.models.generic import BitFlipErrorModel
from qecsim.models.planar import PlanarCode

from qsdxzzx.planarxz import PlanarBiasedPDErrorModel, PlanarBiasedPDDecoder


@pytest.mark.parametrize('degeneracy', [
    True, False, 1, 0,
])
def test_planar_biasedpd_decoder_properties(degeneracy):
    decoder = PlanarBiasedPDDecoder(degeneracy)
    assert isinstance(decoder.label, str)
    assert isinstance(repr(decoder), str)


def test_planar_biasedpd_decoder_decode_invalid_error_model():
    code = PlanarCode(13, 13)
    error_model = BitFlipErrorModel()
    decoder = PlanarBiasedPDDecoder()
    p = 0.1
    error = error_model.generate(code, p)
    syndrome = pt.bsp(error, code.stabilizers.T)
    with pytest.raises(ValueError):
        decoder.decode(code, syndrome, error_model=error_model, error_probability=p)


@pytest.mark.parametrize('primal, degeneracy', [
    (True, True),
    (True, False),
    (False, True),
    (False, False),
])
def test_planar_biasedpd_decoder_decode(primal, degeneracy):
    code = PlanarCode(13, 13)
    error_model = PlanarBiasedPDErrorModel(300, primal)
    decoder = PlanarBiasedPDDecoder(degeneracy)
    p = 0.3
    error = error_model.generate(code, p, np.random.default_rng(5))
    print()
    print(code.new_pauli(error))
    syndrome = pt.bsp(error, code.stabilizers.T)
    recovery = decoder.decode(code, syndrome, error_model=error_model, error_probability=p)
    assert np.array_equal(pt.bsp(recovery, code.stabilizers.T), syndrome), (
        'recovery {} does not give the same syndrome as the error {}'.format(recovery, error))
    assert np.all(pt.bsp(recovery ^ error, code.stabilizers.T) == 0), (
        'recovery ^ error ({} ^ {}) does not commute with stabilizers.'.format(recovery, error))


@pytest.mark.parametrize('size, primal, degeneracy, n_fail', [
    ((5, 23), True, False, 2),  # balanced aspect ratio for bias=30, p=0.25
    ((5, 23), False, False, 2),  # balanced aspect ratio for bias=30, p=0.25
    ((2, 23), True, False, 14),  # less rows should fail more with primal (i.e. applying X)
    ((5, 14), False, False, 13),  # fewer cols should fail more with dual (i.e. applying Z)
    ((5, 14), False, True, 11),  # applying degeneracy term improves previous result slightly
])
def test_planar_biasedpd_decoder_decode_failure_rate(size, primal, degeneracy, n_fail):
    code = PlanarCode(*size)
    error_model = PlanarBiasedPDErrorModel(30, primal)
    decoder = PlanarBiasedPDDecoder(degeneracy)
    error_probability = 0.25
    max_runs = 100
    random_seed = 13
    data = app.run(code, error_model, decoder, error_probability, max_runs, random_seed=random_seed)
    print(data)
    print('n_run', data['n_run'])
    print('n_fail', data['n_fail'])
    print('logical_failure_rate', data['logical_failure_rate'])
    assert data['n_fail'] == n_fail  # regression test
