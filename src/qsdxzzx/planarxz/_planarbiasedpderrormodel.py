import functools
import math

import numpy as np
from qecsim.model import ErrorModel, cli_description


@cli_description('Biased P/D (bias FLOAT > 0, [primal] BOOL)')
class PlanarBiasedPDErrorModel(ErrorModel):
    r"""
    Implements a planar biased-primal/dual error model.

    Notes:

    * By applying this error model to the standard CSS planar code, we simulate
      the effect of a biased-noise model on the XZZX planar code.
    * This error model should be used in conjunction with the code
      :class:`qecsim.models.planar.PlanarCode` and the decoder
      :class:`~qsdxzzx.planarxz.PlanarBiasedPDDecoder`.

    The bias axis is Z and the effective simulated single-qubit probability
    distribution (p_I, p_X, p_Y, p_Z) for the given `bias` parameter is:

    * p_I = 1 - p
    * p_X = 1 / (2 * (bias + 1)) * p: X
    * p_Y = 1 / (2 * (bias + 1)) * p: Y
    * p_Z = bias / (bias + 1) * p: Z

    High-rate and low-rate probabilities are defined as:

    * p_hr = p_Z + p_Y
    * p_lr = p_X + p_Y

    The `primal` parameter determines whether stabilizers on the primal or dual
    lattice are targeted. The key point is that we want to simulate high-rate
    errors forming strings along rows, while low-rate errors form strings along
    columns, as would occur with the XZZX planar code with plaquette and vertex
    stabilizers taking the form, respectively:
    ::

        |--Z--|       Z
        X     X    X--|--X
        |--Z--|       Z

    If primal is specified then X errors are applied targeting primal
    stabilizers and potentially inducing an X-type logical failure. In this
    case, X errors are laid down with the following probabilities:

    * Horizontal edges: p_lr
    * Vertical edges: p_hr

    This is consistent with high-rate errors forming strings along rows since
    primal stabilizers on the CSS and XZZX planar codes are defined,
    respectively:
    ::

        |--Z--|    |--Z--|
        Z     Z    X     X
        |--Z--|    |--Z--|


    Conversely, if dual is specified then Z errors are applied targeting dual
    stabilizers and potentially inducing a Z-type logical failure. In this case,
    Z errors are laid down with the following probabilities:

    * Horizontal edges: p_hr
    * Vertical edges: p_lr

    This is consistent with high-rate errors forming strings along rows since
    primal stabilizers on the CSS and XZZX planar codes are defined,
    respectively:
    ::

           X          Z
        X--|--X    X--|--X
           X          Z

    In addition to the members defined in :class:`qecsim.model.ErrorModel`, it
    provides several specific methods:

    * Get bias: :meth:`bias`.
    * Get primal: :meth:`primal`.
    * Get high-rate probability: :meth:`p_high_rate`.
    * Get low-rate probability: :meth:`p_low_rate`.
    """

    def __init__(self, bias, primal=True):
        """
        Initialise new planar biased-primal/dual error model.

        :param bias: Bias in favour of Z errors relative to X or Y errors.
        :type bias: float
        :param primal: Target primal / dual stabilizers. (default=True)
        :type primal: bool
        :raises ValueError: if bias is not >=0.
        :raises TypeError: if any parameter is of an invalid type.
        """
        try:  # paranoid checking for CLI
            if not (bias > 0 and math.isfinite(bias)):
                raise ValueError('{} valid bias values are number > 0'.format(type(self).__name__))
        except TypeError as ex:
            raise TypeError('{} invalid parameter type'.format(type(self).__name__)) from ex
        self._bias = bias
        self._primal = bool(primal)

    @property
    def bias(self):
        """
        Bias.

        :rtype: float
        """
        return self._bias

    @property
    def primal(self):
        """
        Primal.

        :rtype: bool
        """
        return self._primal

    @functools.lru_cache()
    def p_high_rate(self, p):
        """
        High-rate probability, i.e. p_Y + p_Z.

        :param p: Error probability.
        :type p: float
        :return: High-rate probability.
        :rtype: float
        """
        _, _, p_y, p_z = self.probability_distribution(p)
        return p_y + p_z

    @functools.lru_cache()
    def p_low_rate(self, p):
        """
        Low-rate probability, i.e. p_X + p_Y.

        :param p: Error probability.
        :type p: float
        :return: Low-rate probability.
        :rtype: float
        """
        _, p_x, p_y, _ = self.probability_distribution(p)
        return p_x + p_y

    @functools.lru_cache()
    def probability_distribution(self, probability):
        """See :meth:`qecsim.model.ErrorModel.probability_distribution`"""
        p_x = 1 / (2 * (self._bias + 1)) * probability
        p_y = p_x
        p_z = self._bias / (self._bias + 1) * probability
        p_i = 1 - sum((p_x, p_y, p_z))
        return p_i, p_x, p_y, p_z

    def generate(self, code, probability, rng=None):
        """See :meth:`qecsim.model.ErrorModel.generate`"""
        # generate random ops
        rng = np.random.default_rng() if rng is None else rng
        error_op = 'X' if self.primal else 'Z'
        p_hr = self.p_high_rate(probability)
        p_lr = self.p_low_rate(probability)
        # ensure high-rate spreads errors along rows
        p_horizontal, p_vertical = (p_lr, p_hr) if self.primal else (p_hr, p_lr)
        rows, cols = code.size
        n_horizontal = rows * cols
        n_vertical = (rows - 1) * (cols - 1)
        random_ops_horizontal = rng.choice(('I', error_op), size=n_horizontal, p=(1 - p_horizontal, p_horizontal))
        random_ops_vertical = rng.choice(('I', error_op), size=n_vertical, p=(1 - p_vertical, p_vertical))
        # apply random ops to error
        error_pauli = code.new_pauli()
        # bounds and indices
        max_r, max_c = code.bounds
        i_horizontal, i_vertical = 0, 0
        # columns (iterate horizontal edges)
        for c in range(0, max_c + 1, 2):
            # iterate top to bottom
            for r in range(0, max_r + 1, 2):
                # apply op to site
                error_pauli.site(random_ops_horizontal[i_horizontal], (r, c))
                i_horizontal += 1
        # rows (iterate vertical edges)
        for r in range(1, max_r, 2):
            # iterate left to right
            for c in range(1, max_c, 2):
                # apply op to site
                error_pauli.site(random_ops_vertical[i_vertical], (r, c))
                i_vertical += 1

        return error_pauli.to_bsf()

    @property
    def label(self):
        """See :meth:`qecsim.model.ErrorModel.label`"""
        return 'Planar biased-primal/dual (bias={!r}, primal={!r})'.format(self._bias, self._primal)

    def __repr__(self):
        return '{}({!r}, {!r})'.format(type(self).__name__, self._bias, self._primal)
