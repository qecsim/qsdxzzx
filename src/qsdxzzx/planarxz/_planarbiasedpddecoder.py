import functools
import itertools
import math

from qecsim import graphtools as gt
from qecsim.model import Decoder, cli_description

from qsdxzzx.planarxz import PlanarBiasedPDErrorModel


@cli_description('Biased P/D ([degeneracy] BOOL)')
class PlanarBiasedPDDecoder(Decoder):
    """
    Implements a planar biased-primal/dual decoder.

    Notes:

    * Apply Minimum Weight Perfect Matching (MWPM) with weighting in accordance
      with the error model :class:`~qsdxzzx.planarxz.PlanarBiasedPDErrorModel`.
    * This decoder should be used in conjunction with the code
      :class:`qecsim.models.planar.PlanarCode` and the error model
      :class:`~qsdxzzx.planarxz.PlanarBiasedPDErrorModel`.

    Decoding algorithm:

    * The syndrome is resolved to plaquettes defects using:
      :meth:`qecsim.models.planar.PlanarCode.syndrome_to_plaquette_indices`.
    * For each defect the nearest off-boundary plaquette defect is added using:
      :meth:`qecsim.models.planar.PlanarCode.virtual_plaquette_index`.
    * If the total number of defects is odd an extra virtual off-boundary defect is added.
    * A graph between plaquettes is built with weights given by a distance algorithm.
    * A MWPM algorithm is used to match plaquettes into pairs.
    * A recovery operator is constructed by applying the shortest path between matching plaquette pairs using:
      :meth:`qecsim.models.planar.PlanarPauli.path` and returned.

    Distance algorithm:

    * Steps along rows are weighted by wt_step_hr = log((1-p)/p_high_rate).
    * Steps along columns are weighted by wt_step_lr = log((1-p)/p_low_rate).
    * Distance is defined as steps_along_row * wt_step_hr + steps_along_col * wt_step_lr
    * If degeneracy is specified, the log of the following degeneracy term is subtracted from the distance:
      (steps_along_row + steps_along_col) choose steps_along_row
    """

    def __init__(self, degeneracy=True):
        """
        Initialise new planar biased-primal/dual decoder.

        :param degeneracy: Apply degeneracy term. (default=True)
        :type degeneracy: bool
        """
        self._degeneracy = bool(degeneracy)

    @classmethod
    @functools.lru_cache(maxsize=2 ** 28)  # to handle up to 100x100 codes.
    def _distance(cls, code, a_index, b_index, wt_step_hr, wt_step_lr, degeneracy):
        """Distance function weighted to prefer steps along rows and allow for degeneracy if specified."""
        steps_along_cols, steps_along_rows = map(abs, code.translation(a_index, b_index))
        # steps_along_rows are high-rate, steps_along_cols are low-rate
        separation = steps_along_rows * wt_step_hr + steps_along_cols * wt_step_lr
        # include degeneracy if specified
        degeneracy = cls._degeneracy_term(steps_along_rows, steps_along_cols) if degeneracy else 0
        return separation - degeneracy

    @classmethod
    @functools.lru_cache()
    def _step_weight(cls, p, p_edge):
        """Log of step weight based on overall error probability and high- or low-weight edge probability."""
        return math.log((1 - p) / p_edge)

    @classmethod
    @functools.lru_cache(maxsize=2 ** 15)  # to handle up to 100x100 codes.
    def _degeneracy_term(cls, steps_dir1, steps_dir2):
        """Log of degeneracy term, based on steps along rows and columns, to be subtracted from distance."""
        assert steps_dir1 >= 0 and steps_dir2 >= 0, 'Negative steps in degeneracy term.'

        n = steps_dir1 + steps_dir2
        k = steps_dir1
        if k > n:  # pragma: no cover
            return math.log(0)  # cannot happen
        if 2 * k > n:
            k = n - k
        if k == 0:
            return math.log(1)
        result = math.log(n)
        for i in range(2, k + 1):
            result += math.log(n - i + 1)
            result -= math.log(i)
        return result
        # # C++ implementation courtesy of BJB
        # int n = Dir1 + Dir2;
        # int k = Dir1;
        # if (k > n) return log(0);
        # if (2 * k > n) k = n-k;
        # if (k == 0) return log(1);
        # double result = log(n);
        # for (int i = 2; i <= k; ++i) {
        #     result += log(n-i+1);
        #     result -= log(i);
        # }
        # return result;

    def decode(self, code, syndrome,
               error_model=PlanarBiasedPDErrorModel(0.5),  # noqa: B008
               error_probability=0.1, **kwargs):
        """See :meth:`qecsim.model.Decoder.decode`"""
        if not isinstance(error_model, PlanarBiasedPDErrorModel):
            raise ValueError('Only Planar biased-primal/dual error model supported.')
        # prepare recovery
        recovery_pauli = code.new_pauli()
        # get syndrome indices (and check primality)
        indices = code.syndrome_to_plaquette_indices(syndrome)
        assert len([i for i in indices if code.is_primal(i) != error_model.primal]) == 0, (
            'Syndrome index primality not in accordance with error model')
        # extra virual indices are deliberately well off-boundary to be separate from nearest virtual indices
        extra_vindex = (-9, -10) if error_model.primal else (-10, -9)
        # prepare graph
        graph = gt.SimpleGraph()
        # step-weights for high-rate and low-rate edges
        step_weight_hr = self._step_weight(error_probability, error_model.p_high_rate(error_probability))
        step_weight_lr = self._step_weight(error_probability, error_model.p_low_rate(error_probability))
        # prepare virtual nodes
        vindices = set()
        # add weighted edges between nodes and virtual nodes
        for index in indices:
            vindex = code.virtual_plaquette_index(index)
            vindices.add(vindex)
            distance = self._distance(code, index, vindex, step_weight_hr, step_weight_lr, self._degeneracy)
            graph.add_edge(index, vindex, distance)
        # add extra virtual node if odd number of total nodes
        if (len(indices) + len(vindices)) % 2:
            vindices.add(extra_vindex)
        # add weighted edges between all (non-virtual) nodes
        for a_index, b_index in itertools.combinations(indices, 2):
            distance = self._distance(code, a_index, b_index, step_weight_hr, step_weight_lr, self._degeneracy)
            graph.add_edge(a_index, b_index, distance)
        # add zero weight edges between all virtual nodes
        for a_index, b_index in itertools.combinations(vindices, 2):
            graph.add_edge(a_index, b_index, 0)
        # find MWPM edges {(a, b), (c, d), ...}
        mates = gt.mwpm(graph)
        # iterate edges
        for a_index, b_index in mates:
            # add path to recover
            recovery_pauli.path(a_index, b_index)
        # return recover as bsf
        return recovery_pauli.to_bsf()

    @property
    def label(self):
        """See :meth:`qecsim.model.Decoder.label`"""
        return 'Planar biased-primal/dual (degeneracy={!r})'.format(self._degeneracy)

    def __repr__(self):
        return '{}({!r})'.format(type(self).__name__, self._degeneracy)
