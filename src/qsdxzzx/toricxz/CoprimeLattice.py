import random
import Qubit
import Plaquette


class CoprimeLattice:
    """
    Defines a periodic, coprime Lx(L+1) lattice for the XZZX code. Vertices are
    physical qubits, plaquettes are parity check bits.

    Notes:

    * The dimension L of the Lx(L+1) lattice must be odd.
    * Z errors light up SW and NE plaquettes.
    * X error lights up NW and SE plaquettes.
    * Y error lights up NW, NE, SW and SE plaquettes.
    * Example of X, Y and Z operators acting on qubits at index positions
    (1,1), (1,3) and (3,2), respectively, in a 5x6 lattice.

      (0,0)---(0,1)---(0,2)---(0,3)---(0,4)---(0,5)---(0,6)
        |       |       |       |       |       |       |
        |   .   |       |       |   .   |       |       |
        |       |       |       |       |       |       |
      (1,0)-----X-----(1,2)-----Z-----(1,4)---(1,5)---(1,6)
        |       |       |       |       |       |       |
        |       |   .   |   .   |       |       |       |
        |       |       |       |       |       |       |
      (2,0)---(2,1)---(2,2)---(2,3)---(2,4)---(2,5)---(2,6)
        |       |       |       |       |       |       |
        |       |   .   |   .   |       |       |       |
        |       |       |       |       |       |       |
      (3,0)---(3,1)-----Y-----(3,3)---(3,4)---(3,5)---(3,6)
        |       |       |       |       |       |       |
        |       |   .   |   .   |       |       |       |
        |       |       |       |       |       |       |
      (4,0)---(4,1)---(4,2)---(4,3)---(4,4)---(4,5)---(4,6)
        |       |       |       |       |       |       |
        |       |       |       |       |       |       |
        |       |       |       |       |       |       |
      (5,0)---(5,1)---(5,2)---(5,3)---(5,4)---(5,5)---(5,6)

    Use cases:

    * Apply pauli gate to qubit: :meth:`apply_Y`, :meth:`apply_Z`,
    :meth:`apply_X`.
    * Add Pauli errors into lattice: :meth:`add_inf_noise`,
    :meth:`add_high_rate_Z_noise`, :meth:`add_err_from_dic`.
    * Correct errors: :meth:`correct_inf_Z`, :meth:`correct_given_path`.
    * Check for logical errors: :meth:`is_in_trivial_state_1`,
    :meth:`is_in_trivial_state_2`.
    * Check that state is in code space: :meth:`is_in_code_space`.
    """
    def __init__(self, size):
        """
        Initialisation of Lx(L+1) lattice.

        :param size: Dimension L of Lx(L+1) lattice.
        :type size: int
        """
        assert(isinstance(size, int))
        assert size % 2 == 1
        self.size = size
        self.qubits = [[[] for _ in range(size + 1)] for _ in range(size)]
        self.plaquettes = [[[] for _ in range(size + 1)] for _ in range(size)]
        # Initialise qubits and parity check bits to trivial state.
        for i in range(size):
            for j in range(size + 1):
                self.qubits[i][j] = Qubit.Qubit('I')
                self.plaquettes[i][j] = Plaquette.Plaquette(0)

    def apply_Y(self, i, j):
        """
        Apply Y operator to qubit at position (i,j) in the lattice.

        Note:

        * Y error lights up NW, NE, SW and SE plaquettes.

        :param i: Row position of the qubit
        :type i: int
        :param j: Column position of the qubit
        :type j: int
        """
        L = self.size
        self.qubits[i][j].apply_Y()
        self.plaquettes[(i - 1) % L][(j - 1) % (L + 1)].flip()  # NW
        self.plaquettes[(i - 1) % L][j].flip()  # NE
        self.plaquettes[i][(j - 1) % (L + 1)].flip()  # SW
        self.plaquettes[i][j].flip()  # SE

    def apply_Z(self, i, j):
        """
        Apply Z operator to qubit at position (i,j) in the lattice.

        Note:

        * Z errors light up SW and NE plaquettes.

        :param i: Row position of the qubit
        :type i: int
        :param j: Column position of the qubit
        :type j: int
        """
        L = self.size
        self.qubits[i][j].apply_Z()
        self.plaquettes[(i - 1) % L][j].flip()  # NE
        self.plaquettes[i][(j - 1) % (L + 1)].flip()  # SW

    def apply_X(self, i, j):
        """
        Apply X operator to qubit at position (i,j) in the lattice.

        Note:

        * X error lights up NW and SE plaquettes.

        :param i: Row position of the qubit
        :type i: int
        :param j: Column position of the qubit
        :type j: int
        """
        L = self.size
        self.qubits[i][j].apply_X()
        self.plaquettes[(i - 1) % L][(j - 1) % (L + 1)].flip()  # NW
        self.plaquettes[i][j].flip()  # SE

    def get_Z_diagonal(self):
        """
        Get the Z symmetry diagonal of the lattice.

        Note:

        * The Z symmetry diagonal of a coprime Lx(L+1) lattice is the whole
        lattice.

        :return: The Z symmetry diagonal of the lattice traversed in this
        direction: // , as a list of coordinates in the form: diagonal=
        [(x1,y1), (x2,y2), ...]
        :rtype: List of tuple of int
        """
        L = self.size
        Z_diagonal = []
        (x, y) = (0, 0)
        for _ in range(L * (L + 1)):
            Z_diagonal.append((x, y))
            x, y = (x - 1) % L, (y + 1) % (L + 1)
        return Z_diagonal

    def X_diagonal(self):
        """
        Get the X symmetry diagonal of the lattice.

        Note:

        * The X symmetry diagonal of a coprime Lx(L+1) lattice is the whole
        lattice.

        :return: The X symmetry diagonal of the lattice traversed in this
        direction: \\ , as a list of coordinates in the form: diagonal=
        [(x1,y1), (x2,y2), ...]
        :rtype: List of tuple of int
        """
        L = self.size
        X_diagonal = []
        (x, y) = (0, 0)
        for _ in range(L * (L + 1)):
            X_diagonal.append((x, y))
            x, y = (x + 1) % L, (y + 1) % (L + 1)
        return X_diagonal

    def add_inf_noise(self, type, p):
        """
        Add infinite bias Pauli noise.

        :param noise: Type of Pauli noise. Either 'X', 'Y' or 'Z'
        :type noise: str
        :param p: Probability of applying the error to any one qubit.
        :type p: float
        """
        L = self.size
        for i in range(L):
            for j in range(L + 1):
                if random.random() <= p:
                    if type == 'X':
                        self.apply_X(i, j)
                    elif type == 'Y':
                        self.apply_Y(i, j)
                    elif type == 'Z':
                        self.apply_Z(i, j)

    def add_high_rate_Z_noise(self, p, n):
        """
        Add biased dephasing (Z) Pauli noise.

        :param p: Probability of applying an error to any one qubit.
        :type p: float
        :param n: Bias coefficient
        :type n: float
        """
        # Calculate probabilities of each of a Z, X and Y error.
        pz = (p * n) / (n + 1)
        px = (p) / (2 * (n + 1))
        py = (p) / (2 * (n + 1))
        normaliser = 1 / p
        pz_n = pz * normaliser
        py_n = py * normaliser
        px_n = px * normaliser
        for i in range(self.size):
            for j in range(self.size + 1):
                # Check if error will be applied to the qubit at index (i,j).
                if random.random() <= p:
                    # Check what type of error will be applied.
                    r = random.random()
                    if r <= pz_n:
                        self.apply_Z(i, j)
                    elif pz_n < r <= (pz_n + px_n):
                        self.apply_X(i, j)
                    elif (px_n + pz_n) < r <= (pz_n + px_n + py_n):
                        self.apply_Y(i, j)

    def add_err_from_dic(self, dic):
        """
        Adds Pauli errors onto the lattice from a dictionary of
        {coordinate: Pauli state} items.

        :param dic: Dictionary lattice containing the Pauli state of qubits in
        the form {(x, y) = Pauli state}, where Pauli state = 'X', 'Y' or 'Z'.
        """
        for i, j in dic:
            if dic[(i, j)] == 'X':
                self.apply_X(i, j)
            elif dic[(i, j)] == 'Y':
                self.apply_Y(i, j)
            elif dic[(i, j)] == 'Z':
                self.apply_Z(i, j)
            else:
                assert dic[(i, j)] == 'I'

    def correct_inf_Z(self, u, v):
        """
        Correct infinite Z noise by matching plaquette defects along the
        diagonal symmetries of the lattice.

        :param u: Coordinate of one defect in the form 'x1,y1'.
        :type u: str
        :param v: Coordinate of the other defect in the form 'x2,y2'.
        :type v: str
        """
        L = self.size
        x1, y1 = u.split(',')
        x2, y2 = v.split(',')
        x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)

        Z_diagonal = self.get_Z_diagonal()
        indx_u, indx_v = Z_diagonal.index((x1, y1)), Z_diagonal.index((x2, y2))

        # Start at defect u. Check whether to move left or right along
        # diagonal.
        left_steps = (indx_u - indx_v) % (L * (L + 1))
        right_steps = (indx_v - indx_u) % (L * (L + 1))

        if left_steps <= right_steps:  # Move left along diagonal.
            i, j = (x1 + 1) % L, y1
            for _ in range((left_steps)):
                self.apply_Z(i, j)
                i, j = (i + 1) % L, (j - 1) % (L + 1)

        else:  # Move right along diagonal
            i, j = x1, (y1 + 1) % (L + 1)
            for _ in range((right_steps)):
                self.apply_Z(i, j)
                i, j = (i - 1) % L, (j + 1) % (L + 1)

    def correct_given_path(self, u, v, final_path):
        """
        Correct biased Z noise by matching plaquette defects along a
        pre-determined path.

        :param u: Coordinate of one defect in the form 'x1,y1'.
        :type u: str
        :param v: Coordinate of the other defect in the form 'x2,y2'.
        :type v: str
        :param final_path: List of pre-determined correction path, [direction1,
        moves1,direction2,moves2,weight]. direction1 is the initial path
        direction along the X symmetry diagonals (\\), moving SE if direction=
        "R" or NW if direction="L", or no movement if direction="O". moves1 is
        the number of moves along the diagonal (equals 0 if direction1="O").
        direction2 is the subsequent path direction along the Z symmetry
        diagonals (//), moving NE if direction="R" or SW if direction="L", or
        no movement if direction="O". moves2 is the number of moves along the
        diagonal (equals 0 if direction2="O"). weight is the weight of the
        correction path.
        :type final_path: list of str, int, str, int, float
        """
        L = self.size
        x1, y1 = u.split(',')
        x2, y2 = v.split(',')
        x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)

        # Correct along X diagonals (\\).
        x, y = x1, y1  # Initially at u.
        if final_path[0] == "L":
            left_X_steps = final_path[1]
            assert left_X_steps != 0
            for _X_step in range(left_X_steps):
                self.apply_X(x, y)
                x, y = (x - 1) % L, (y - 1) % (L + 1)
        elif final_path[0] == "R":
            right_X_steps = final_path[1]
            assert right_X_steps != 0
            for _X_step in range(right_X_steps):
                self.apply_X((x + 1) % L, (y + 1) % (L + 1))
                x, y = (x + 1) % L, (y + 1) % (L + 1)
        else:
            assert final_path[0] == "O"
            assert final_path[1] == 0

        # Now correct along X diagonals.
        if final_path[2] == "L":
            left_Z_steps = final_path[3]
            assert left_Z_steps != 0
            for _Z_step in range(left_Z_steps):
                self.apply_Z((x + 1) % L, y)
                x, y = (x + 1) % L, (y - 1) % (L + 1)
        elif final_path[2] == "R":
            right_Z_steps = final_path[3]
            assert right_Z_steps != 0
            for _Z_step in range(right_Z_steps):
                self.apply_Z(x, (y + 1) % (L + 1))
                x, y = (x - 1) % L, (y + 1) % (L + 1)
        else:
            assert final_path[2] == "O"
            assert final_path[3] == 0

        # Ensure that we arrived at the second plaquette defect.
        assert (x, y) == (x2, y2)

    def is_in_trivial_state_1(self):
        """
        Checks for type 1 logical operators. These are rows of
        alternating X and Z operators (XZXZ) or any muplication of these
        by stabilisers.

        :return: Whether it is in trivial state with respect to the logical
        operators or not.
        :rtype: Bool
        """
        L = self.size
        # Check parity along each column.
        for j in range(L + 1):
            total = 0  # Z, X: +1 , Y: +2
            for i in range(L):
                if self.qubits[i][j].state == "Z" or self.qubits[i][j].state == "X":
                    total += 1
                if self.qubits[i][j].state == "Y":
                    total += 2
            if total % 2 == 1:
                return False
        return True

    def is_in_trivial_state_2(self):
        """
        Checks for type 2 logical operators. These are columns of Y
        operators (YYY) or any muplication of these by stabilisers. It also
        detects type 1 logical operators.

        :return: Whether it is in trivial state with respect to the logical
        operators or not.
        :rtype: Bool
        """
        L = self.size
        total = 0  # Z, Y : +1
        for i in range(L):
            for j in range(L + 1):
                if self.qubits[i][j].state == "Z" or self.qubits[i][j].state == "Y":
                    total += 1
        if total % 2 == 1:
            return False
        return True

    def is_in_code_space(self):
        """
        Checks whether the state of the lattice is in the code space and
        therefore suitable for making an inference about its logical error
        state.

        :return: Whether it is in the code space or not.
        :rtype: Bool
        """
        L = self.size
        for i in range(L):
            for j in range(L + 1):
                if self.plaquettes[i][j].state == 1:
                    return False
        return True

    def __repr__(self):
        strn = []
        for i in range(self.size):
            for j in range(self.size + 1):
                strn.append(self.qubits[i][j].state)
            strn.append('\n')
        print(strn)
        return "".join(strn)
