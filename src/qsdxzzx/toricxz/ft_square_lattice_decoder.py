import os
import time
import math
import pickle
import random
import sys

import SquareLattice as sl
import generate_paths_square_lattice as gp
import qecsim.graphtools as gt


def measure_err(L, q):
    """
    Simulates measurement errors on the stabilisers of the lattice.

    :param L: Size of LxL lattice.
    :type L: int
    :param q: Probability of a measurement error on any one stabiliser of the
    lattice, which can be associated with any one plaquette.
    :type q: float
    :return: A lattice where certain plaquettes have been changed from their
    trivial 0 state to the 1 state, simulating measurement errors as well as
    the number of measurement errors that occurred.
    :rtype: tuple of object and int
    """
    assert 0 <= q and q <= 1
    # Store the error by flipping the state of plaquettes in a SquareLattice
    # object.
    result = sl.SquareLattice(L)
    tot_err_occurred = 0
    for i in range(L):
        for j in range(L):
            if random.random() <= q:
                result.plaquettes[i][j].state = 1
                tot_err_occurred += 1
            else:
                assert result.plaquettes[i][j].state == 0
    return (result, tot_err_occurred)


def err_products(lattice3d):
    """
    Calculates the product of Pauli errors on each surface code of a
    (2+1)-dimensional lattice where the last dimension is time. It flattens
    the net error onto a 2D surface code.

    :return: A square lattice containing the product of all the errors on the
    surface codes making up the (2+1)-dimensional structure.
    :rtype: object
    """
    L = lattice3d[0].size
    T = len(lattice3d)
    result = sl.SquareLattice(L)
    for k in range(T):
        for i in range(L):
            for j in range(L):
                if lattice3d[k].qubits[i][j].state == 'X':
                    result.apply_X(i, j)
                elif lattice3d[k].qubits[i][j].state == 'Y':
                    result.apply_Y(i, j)
                elif lattice3d[k].qubits[i][j].state == 'Z':
                    result.apply_Z(i, j)
    return result


def stack_up(p, L, T, bias, q):
    """
    Models Pauli errors on a 2D surface code at each time step as well as
    measurement errors on the stabiliser read-outs and stacks the results
    into a (2+1)-dimensional lattice.

    :param p: Probability of an error on any one physical qubit at any one
    time.
    :type p: float
    :param L: Dimension of LxL surface code.
    :type L: int
    :param T: Number of time steps to run the simulations until.
    :type T: int
    :param bias: Bias coefficient of noise towards dephasing.
    :type bias: float
    :param q: Probability of a measurement error on any one stabiliser of the
    lattice, which can be associated with any one plaquette.
    :type q: float
    :return: The flattened 2D surface code of the product of all Pauli error
    on each of the stacked 2D surface codes over time, the number of
    measurement errors at each time step and the stacked (2+1)-dimensional
    lattice of Pauli and measurement errors at each time step.
    :rtype: tuple of objects
    """
    err_at_T = []  # Keeps the NEW error syndromes at each time step.
    tot_err = []  # Records the number of measurement errors at each time step.
    meas_err = []  # Records lattice of measurement errors at each time step.
    lattice3d = []  # Builds the 3d lattice with differences between defects.

    if bias == 'inf':
        for _ in range(T):
            lattice = sl.SquareLattice(L)
            lattice.add_inf_noise('Z', p)
            err_at_T.append(lattice)
            lattice3d.append(lattice)
            # Model measurement errors.
            measured, errT = measure_err(L, q)
            meas_err.append(measured)
            tot_err.append(errT)
    else:
        for _ in range(T):
            lattice = sl.SquareLattice(L)
            lattice.add_high_rate_Z_noise(p, bias)
            err_at_T.append(lattice)
            lattice3d.append(lattice)
            # Model measurement errors.
            measured, errT = measure_err(L, q)
            meas_err.append(measured)
            tot_err.append(errT)

    # From recorded measurement errors build the 3d lattice.
    for k in range(T):
        err = meas_err[k]
        for i in range(L):
            for j in range(L):
                # If there was a measurement error between 2 time stamps on a
                # certain plaquette, flip the plaquette state on each of time
                # stamps.
                if err.plaquettes[i][j].state == 1:
                    lattice3d[k].plaquettes[i][j].state ^= 1
                    lattice3d[(k - 1) % T].plaquettes[i][j].state ^= 1
    # Calculate flattened 2D error product of all Pauli errors at each time
    # step.
    ep = err_products(err_at_T)

    return (ep, tot_err, lattice3d)


def crossed_times(z, w, T):
    """
    Returns the number of time steps crossed by the mwpm path between 2
    defects given their time coordinate.

    :param z: Time coordinate of first defect.
    :type z: int
    :param w: Time coordinate of second defect.
    :type w: int
    :param T: Number of time steps the simulations are run to.
    :type T: int
    :return: The number of times crossed by the mwpm path between the defects.
    :rtype: int
    """
    assert z != w
    z1, z2 = min([z, w]), max([z, w])
    assert z2 > z1
    crs_t = []
    if z2 - z1 < T - (z2 - z1):
        for j in range(z1 + 1, z2 + 1):
            crs_t.append(j)
    else:
        for j in range(z2 + 1, T):
            crs_t.append(j)
        for j in range(z1 + 1):
            crs_t.append(j)
    return crs_t


def decode_ft_high_rate_Z(tot_err_lat, lattice3d, p, q, bias, paths_dic):
    """
    Conduct mwpm in the (2+1)-dimensional lattice of space and time defects
    assuming a biased noise model toward dephasing.

    :param tot_err_lat: Flattened 2D error product of all Pauli errors at each
    time step.
    :type tot_err_lat: object
    :param lattice3d: Stacked (2+1)-dimensional lattice of Pauli and
    measurement errors at each time step.
    :type lattice3d: object
    :param p: Probability of an error on any one physical qubit at any one
    time.
    :type p: float
    :param q: Probability of a measurement error on any one stabiliser of the
    lattice, which can be associated with any one plaquette.
    :type q: float
    :param bias: Bias coefficient of noise towards dephasing.
    :type bias: float
    :param paths_dic: Dictionary of pre-determined weights between any two
    defects in the 2D space lattice. Keys of the form (coord1,coord2) where
    coord=(x,y). Value of the form: weight.
    :type paths_dic: dict
    :return: The corrected flattened 2D error product of all Pauli errors at
    each time step and the number of times matched paths cross each time step
    as a list: tot_errT = [total crossings through 1st time step,
    total crossings through 2nd time step, ...]
    :rtype: tuple of object and list (of int)
    """
    T = len(lattice3d)
    L = lattice3d[0].size
    time_weight = -math.log(q / (1 - q))

    # Match along the even and odd parity defects (in terms of the sum of x
    # and y coordinates) separately.
    nodes1 = []
    nodes2 = []
    for k in range(T):
        for i in range(L):
            for j in range(L):
                if lattice3d[k].plaquettes[i][j].state == 1:
                    if (i + j) % 2 == 0:
                        nodes1.append((i, j, k))
                    else:
                        nodes2.append((i, j, k))
    graph1 = gt.SimpleGraph()
    for v1 in nodes1:
        for v2 in nodes1:
            if v1 != v2:
                x1, y1, z1, x2, y2, z2 = v1[0], v1[1], v1[2], v2[0], v2[1], v2[2]
                if (x1, y1) == (x2, y2):
                    graph1.add_edge(str(x1) + "," + str(y1) + "," + str(z1), str(x2) + "," + str(y2) + "," + str(z2),
                                    time_weight * min([(z1 - z2) % T, (z2 - z1) % T]))
                else:
                    path_xy = paths_dic[((x2 - x1) % L, (y2 - y1) % (L))]
                    weight = path_xy[4] + time_weight * min([(z1 - z2) % T, (z2 - z1) % T])
                    graph1.add_edge(str(x1) + "," + str(y1) + "," + str(z1),
                                    str(x2) + "," + str(y2) + "," + str(z2), weight)

    graph2 = gt.SimpleGraph()
    for v1 in nodes2:
        for v2 in nodes2:
            if v1 != v2:
                x1, y1, z1, x2, y2, z2 = v1[0], v1[1], v1[2], v2[0], v2[1], v2[2]
                if (x1, y1) == (x2, y2):
                    graph2.add_edge(str(x1) + "," + str(y1) + "," + str(z1), str(x2) + "," + str(y2) + "," + str(z2),
                                    time_weight * min([(z1 - z2) % T, (z2 - z1) % T]))
                else:
                    path_xy = paths_dic[((x2 - x1) % L, (y2 - y1) % (L))]
                    weight = path_xy[4] + time_weight * min([(z1 - z2) % T, (z2 - z1) % T])
                    graph2.add_edge(str(x1) + "," + str(y1) + "," + str(z1),
                                    str(x2) + "," + str(y2) + "," + str(z2), weight)

    matching = list(gt.mwpm(graph1)) + list(gt.mwpm(graph2))

    # Number of times a matched path crosses each time step. Index of list
    # indicates time step.
    tot_errT = [0 for j in range(T)]

    for u3, v3 in matching:  # matching = [('x1,y1,z1', 'x2,y2,z2'), ... ]
        x1, y1, z1 = u3.split(",")
        x2, y2, z2 = v3.split(",")
        if z1 != z2:
            # Times crossed by matching.
            crs_t = crossed_times(int(z1), int(z2), T)
            for t in crs_t:
                tot_errT[t] += 1
        # Correct the flattened 2D lattice and check for spatial logical
        # errors.
        if (x1, y1) != (x2, y2):
            x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)
            path_xy = paths_dic[((x2 - x1) % L, (y2 - y1) % (L))]
            tot_err_lat.correct_given_path(str(x1) + "," + str(y1), str(x2) + "," + str(y2), path_xy)

    return (tot_err_lat, tot_errT)


def decode_ft_inf_Z_bias(tot_err_lat, lattice3d, p, q):
    """
    Conduct mwpm in the (2+1)-dimensional lattice of space and time defects
    under a purely dephasing noise model.

    :param tot_err_lat: Flattened 2D error product of all Pauli errors at each
    time step.
    :type tot_err_lat: object
    :param lattice3d: Stacked (2+1)-dimensional lattice of Pauli and
    measurement errors at each time step.
    :type lattice3d: object
    :param p: Probability of an error on any one physical qubit at any one
    time.
    :type p: float
    :param q: Probability of a measurement error on any one stabiliser of the
    lattice, which can be associated with any one plaquette.
    :type q: float
    :param bias: Bias coefficient of noise towards dephasing.
    :return: The corrected flattened 2D error product of all Pauli errors at
    each time step and the number of times matched paths cross each time step
    as a list: tot_errT = [total crossings through 1st time step,
    total crossings through 2nd time step, ...]
    :rtype: tuple of object and list (of int)
    """
    T = len(lattice3d)
    L = lattice3d[0].size

    # Match along the Z symmetry diagonal sheets (//).
    diagonals = lattice3d[0].diagonals_to_list()
    matching = []

    for d in diagonals:
        nodes = []
        for k in range(T):
            for i, j in d:
                if lattice3d[k].plaquettes[i][j].state == 1:
                    nodes.append((i, j, k))
        graph = gt.SimpleGraph()
        for v1 in nodes:
            for v2 in nodes:
                if v1 != v2:
                    x1, y1, z1, x2, y2, z2 = v1[0], v1[1], v1[2], v2[0], v2[1], v2[2]
                    if (x1, y1) == (x2, y2):
                        graph.add_edge(str(x1) + "," + str(y1) + "," + str(z1),
                                       str(x2) + "," + str(y2) + "," + str(z2), min([(z1 - z2) % T, (z2 - z1) % T]))
                    else:
                        spatial_weight = min([(y1 - y2) % L, (y2 - y1) % L])
                        time_weight = min([(z1 - z2) % T, (z2 - z1) % T])
                        graph.add_edge(str(x1) + "," + str(y1) + "," + str(z1),
                                       str(x2) + "," + str(y2) + "," + str(z2), spatial_weight + time_weight)

        matching += list(gt.mwpm(graph))

    # Number of times a matched path crosses each time step. Index of list
    # indicates time step.
    tot_errT = [0 for j in range(T)]

    for u3, v3 in matching:  # matching = [('x1,y1,z1', 'x2,y2,z2'), ...]
        x1, y1, z1 = u3.split(",")
        x2, y2, z2 = v3.split(",")
        if z1 != z2:
            # Times crossed by matching.
            crs_t = crossed_times(int(z1), int(z2), T)
            for t in crs_t:
                tot_errT[t] += 1
        # Correct the flattened 2D lattice and check for spatial logical
        # errors.
        if (x1, y1) != (x2, y2):
            x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)
            tot_err_lat.correct_inf_Z(str(x1) + "," + str(y1), str(x2) + "," + str(y2))

    return (tot_err_lat, tot_errT)


def test(p, bias, L, N, q='standard', keep_paths_dic=False):
    """
    Run N simulations of error input and error correction on an LxL surface
    code stacked over T=L time steps.

    :param p: Probability of an error on any one physical qubit at any one
    time.
    :type p: float
    :param bias: Bias coefficient. If bias equals 'inf' then noise is
    infinitely biased, otherwise bias is a float greater than 0
    quantifying the bias toward dephasing.
    :type bias: str or float
    :param L: Size of LxL lattice. Must be an even number.
    :type L: int
    :param N: Number of simulations to run.
    :type N: int
    :param q: Probability of a measurement error on any one stabiliser of the
    lattice, which can be associated with any one plaquette. If q equals
    'standard' then its value is chosen in the same way as in the article
    'The XZZX surface code'.
    :type q: str ('standard') or float
    :param keep_paths_dic: Whether to keep the created path dictionary in the
    current folder or delete it.
    :type keep_paths_dic: boolean
    """
    start_time = time.time()  # Time simulations.

    assert L % 2 == 0

    if q == 'standard' and bias == 'inf':
        q = p
    elif q == 'standard' and bias != 'inf':
        pZ = (p * bias) / (bias + 1)
        pX = p / (2 * (bias + 1))
        q = pZ + pX

    if bias != 'inf':
        # Load or create path file
        filename = "L_L_distance_table_" + str(L) + "_" + str(bias) + "_" + str(p) + ".pkl"
        if not os.path.isfile(filename):
            gp.generate_path(L, bias, p)

        pkl_file = open("L_L_distance_table_" + str(L) + "_" + str(bias) + "_" + str(p) + ".pkl", 'rb')
        paths_dic = pickle.load(pkl_file)
        pkl_file.close()

    # Make the time dimension the same as the surface code dimension so that
    # the (2+1)-dimensional structure is a cube.
    T = L

    # Simulate errors and correction.
    total_fail = 0
    for _ in range(N):
        temp_fail = False
        spat_fail = False
        tot_err_lat, temp_errs, lattice3d = stack_up(p, L, T, bias, q)
        if bias != 'inf':
            result_lat, tot_errT = decode_ft_high_rate_Z(tot_err_lat, lattice3d, p, q, bias, paths_dic)
        else:
            result_lat, tot_errT = decode_ft_inf_Z_bias(tot_err_lat, lattice3d, p, q)
        assert result_lat.is_in_code_space()
        # Check for spatial logical errors.
        if (not result_lat.is_in_trivial_state()) or (not result_lat.is_in_trivial_state_Z()):
            spat_fail = True
        # Check for temporal logical errors which arise if the parity of the
        # number of measurement errors and the parity of crossings with MWPM
        # at each time step are not equal.
        for j in range(T):
            if temp_errs[j] % 2 != tot_errT[j] % 2:
                temp_fail = True
                break
        if spat_fail or temp_fail:
            total_fail += 1

    print([{"code: Rotated square XZ " + str(L) + "x" + str(L),
            "decoder: Rotated square XZ MWPM",
            "error_model: Biased noise toward dephasing",
            "bias: " + str(bias),
            "error_probability: " + str(p),
            "logical_failure_rate: " + str(total_fail / N),
            "measurement_error_probability: " + str(q),
            "n_run: " + str(N),
            "n_fail: " + str(total_fail),
            "wall_time: " + str(time.time() - start_time)}])
    if bias != 'inf':
        if not keep_paths_dic:
            os.remove("L_L_distance_table_" + str(L) + "_" + str(bias) + "_" + str(p) + ".pkl")


if __name__ == "__main__":

    try:
        assert len(sys.argv) >= 5
        all_arguments = []
        for i in range(len(sys.argv)):
            if i > 0:
                argument = sys.argv[i]
                key, value = argument.split('=')
                if key == "p":
                    all_arguments.append("p")
                    p = float(value)
                elif key == "bias":
                    all_arguments.append("bias")
                    if value == 'inf':
                        bias = 'inf'
                    else:
                        bias = float(value)
                elif key == "L":
                    all_arguments.append("L")
                    L = int(value)
                    assert L % 2 == 0
                elif key == "N":
                    all_arguments.append("N")
                    N = int(value)
                elif key == "q":
                    q = float(value)
                else:
                    assert key == "keep_paths_dic"
                    keep_paths_dic = bool(value)
        assert all(item in all_arguments for item in ["p", "bias", "L", "N"])

    except Exception:
        print("#######################")
        print()
        print("Incorrect input syntax.")
        print(".......................")
        print("Run this program as: ")
        print()
        print("python ft_square_lattice_decoder.py p=<physical_error_rate>"
              + " bias=<bias_coefficient> L=<size_of_square_lattice>"
              + " N=<tot_number_of_simulations>")
        print()
        print("<physical_error_rate>: float between 0 and 1 (inclusive)")
        print("<bias_coefficient>: float greater than 0. Can also be str"
              + " 'inf' to use a noise model infinitely biased toward dephasing")
        print("<size_of_square_lattice>: int positive and even.")
        print("<tot_number_of_simulations>: int greater than zero.")
        print(".......................")
        print("Optional arguments:")
        print("q=<float>: The probability of a measurement error from any"
              + "one stabiliser at any one time stamp. The default is the"
              + "value used in the article 'The XZZX Surface Code' arXiv:2009:07851")
        print("keep_paths_dic=<Boolean (True or False)>: Whether to keep "
              + "the look-up tables used to assign the weight between two "
              + "defects for the MWPM step or not. The default is False.")
        print(".......................")
        print("Example: ")
        print("python ft_square_lattice_decoder.py p=0.055 bias=10 L=12 N=10")
        print()
        print("#######################")

        sys.exit()

    try:
        q
    except NameError:
        q = 'standard'

    try:
        keep_paths_dic
    except NameError:
        keep_paths_dic = False

    test(p, bias, L, N, q, keep_paths_dic)
