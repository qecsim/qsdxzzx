import random
import Qubit
import Plaquette


class SquareLattice:
    """
    Defines a periodic, square lattice for the XZZX code. Vertices are
    physical qubits, plaquettes are parity check bits.

    Notes:

    * The dimension L of the LxL lattice must be even.
    * Z errors light up SW and NE plaquettes.
    * X error lights up NW and SE plaquettes.
    * Y error lights up NW, NE, SW and SE plaquettes.
    * Example of X, Y and Z operators acting on qubits at index positions
    (1,1), (1,3) and (3,2), respectively.

      (0,0)---(0,1)---(0,2)---(0,3)---(0,4)
        |       |       |       |       |
        |   .   |       |       |   .   |
        |       |       |       |       |
      (1,0)-----X-----(1,2)-----Z-----(1,4)
        |       |       |       |       |
        |       |   .   |   .   |       |
        |       |       |       |       |
      (2,0)---(2,1)---(2,2)---(2,3)---(2,4)
        |       |       |       |       |
        |       |   .   |   .   |       |
        |       |       |       |       |
      (3,0)---(3,1)-----Y-----(3,3)---(3,4)
        |       |       |       |       |
        |       |   .   |   .   |       |
        |       |       |       |       |
      (4,0)---(4,1)---(4,2)---(4,3)---(4,4)

    Use cases:

    * Apply pauli gate to qubit: :meth:`apply_Y`, :meth:`apply_Z`,
    :meth:`apply_X`.
    * Add Pauli errors into lattice: :meth:`add_inf_noise`,
    :meth:`add_high_rate_Z_noise`.
    * Correct errors: :meth:`correct_inf_Z`, :meth:`correct_given_path`.
    * Check for logical errors: :meth:`is_in_trivial_state`,
    :meth:`is_in_trivial_state_Z`.
    * Check that state is in code space: :meth:`is_in_code_space`.
    """
    def __init__(self, size):
        """
        Initialisation of LxL lattice.

        :param size: Dimension L of LxL lattice.
        :type size: int
        """
        assert size % 2 == 0
        self.size = size
        self.qubits = [[[] for _ in range(size)] for _ in range(size)]
        self.plaquettes = [[[] for _ in range(size)] for _ in range(size)]
        # Initialise qubits and parity check bits to trivial state.
        for i in range(size):
            for j in range(size):
                self.qubits[i][j] = Qubit.Qubit('I')
                self.plaquettes[i][j] = Plaquette.Plaquette(0)

    def apply_Y(self, i, j):
        """
        Apply Y operator to qubit at position (i,j) in the lattice.

        Note:

        * Y error lights up NW, NE, SW and SE plaquettes.

        :param i: Row position of the qubit
        :type i: int
        :param j: Column position of the qubit
        :type j: int
        """
        L = self.size
        self.qubits[i][j].apply_Y()
        self.plaquettes[(i - 1) % L][(j - 1) % L].flip()  # NW
        self.plaquettes[(i - 1) % L][j].flip()  # NE
        self.plaquettes[i][(j - 1) % L].flip()  # SW
        self.plaquettes[i][j].flip()  # SE

    def apply_Z(self, i, j):
        """
        Apply Z operator to qubit at position (i,j) in the lattice.

        Note:

        * Z errors light up SW and NE plaquettes.

        :param i: Row position of the qubit
        :type i: int
        :param j: Column position of the qubit
        :type j: int
        """
        L = self.size
        self.qubits[i][j].apply_Z()
        self.plaquettes[(i - 1) % L][j].flip()  # NE
        self.plaquettes[i][(j - 1) % L].flip()  # SW

    def apply_X(self, i, j):
        """
        Apply X operator to qubit at position (i,j) in the lattice.

        Note:

        * X error lights up NW and SE plaquettes.

        :param i: Row position of the qubit
        :type i: int
        :param j: Column position of the qubit
        :type j: int
        """
        L = self.size
        self.qubits[i][j].apply_X()
        self.plaquettes[(i - 1) % L][(j - 1) % L].flip()  # NW
        self.plaquettes[i][j].flip()  # SE

    def add_inf_noise(self, noise, p):
        """
        Add infinite bias Pauli noise.

        :param noise: Type of Pauli noise. Either 'X', 'Y' or 'Z'
        :type noise: str
        :param p: Probability of applying the error to any one qubit.
        :type p: float
        """
        L = self.size
        for i in range(L):
            for j in range(L):
                if random.random() <= p:
                    if noise == 'X':
                        self.apply_X(i, j)
                    elif noise == 'Y':
                        self.apply_Y(i, j)
                    elif noise == 'Z':
                        self.apply_Z(i, j)

    def add_high_rate_Z_noise(self, p, n):
        """
        Add biased dephasing (Z) Pauli noise.

        :param p: Probability of applying an error to any one qubit.
        :type p: float
        :param n: Bias coefficient
        :type n: float
        """
        # Calculate probabilities of each of a Z, X and Y error.
        pz = (p * n) / (n + 1)
        px = (p) / (2 * (n + 1))
        py = (p) / (2 * (n + 1))
        # Normalise to the probability of an error, p
        normaliser = 1 / p
        pz_n = pz * normaliser
        py_n = py * normaliser
        px_n = px * normaliser
        for i in range(self.size):
            for j in range(self.size):
                # Check if error will be applied to the qubit at index (i,j).
                if random.random() <= p:
                    # Check what type of error will be applied.
                    r = random.random()
                    if r <= pz_n:
                        self.apply_Z(i, j)
                    elif pz_n < r <= (pz_n + px_n):
                        self.apply_X(i, j)
                    elif (px_n + pz_n) < r <= (pz_n + px_n + py_n):
                        self.apply_Y(i, j)

    def correct_inf_Z(self, u, v):
        """
        Correct infinite Z noise by matching plaquette defects along the
        diagonal symmetries of the lattice.

        :param u: Coordinate of one defect in the form 'x1,y1'.
        :type u: str
        :param v: Coordinate of the other defect in the form 'x2,y2'.
        :type v: str
        """
        L = self.size
        x1, y1 = u.split(',')
        x2, y2 = v.split(',')
        x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)
        # Set (x1,y1) as the coordinates of the left-most point.
        if y2 < y1:
            y1, y2 = y2, y1
            x1, x2 = x2, x1
        # Check whether it is necessary to wrap around lattice.
        if y2 - y1 <= L - (y2 - y1):
            for j in range(y2 - y1):
                self.apply_Z((x1 - j) % L, (y1 + 1 + j) % L)
        else:
            for j in range(L - (y2 - y1)):
                self.apply_Z((x1 + 1 + j) % L, (y1 - j) % L)

    def correct_given_path(self, u, v, final_path):
        """
        Correct biased Z noise by matching plaquette defects along a
        pre-determined path.

        :param u: Coordinate of one defect in the form 'x1,y1'.
        :type u: str
        :param v: Coordinate of the other defect in the form 'x2,y2'.
        :type v: str
        :param final_path: List of pre-determined correction path, [direction1,
        moves1,direction2,moves2,weight]. direction1 is the initial path
        direction along the X symmetry diagonals (\\), moving SE if direction=
        "R" or NW if direction="L", or no movement if direction="O". moves1 is
        the number of moves along the diagonal (equals 0 if direction1="O").
        direction2 is the subsequent path direction along the Z symmetry
        diagonals (//), moving NE if direction="R" or SW if direction="L", or
        no movement if direction="O". moves2 is the number of moves along the
        diagonal (equals 0 if direction2="O"). weight is the weight of the
        correction path.
        :type final_path: list of str, int, str, int, float
        """
        L = self.size
        x1, y1 = u.split(',')
        x2, y2 = v.split(',')
        x1, y1, x2, y2 = int(x1), int(y1), int(x2), int(y2)

        # Correct along X diagonals (\\).
        x, y = x1, y1  # Initially at u.
        if final_path[0] == "L":
            left_X_steps = final_path[1]
            assert left_X_steps != 0
            for _X_step in range(left_X_steps):
                self.apply_X(x, y)
                x, y = (x - 1) % L, (y - 1) % L
        elif final_path[0] == "R":
            right_X_steps = final_path[1]
            assert right_X_steps != 0
            for _X_step in range(right_X_steps):
                self.apply_X((x + 1) % L, (y + 1) % L)
                x, y = (x + 1) % L, (y + 1) % L
        else:
            assert final_path[0] == "O"
            assert final_path[1] == 0

        # Correct along Z diagonals (//).
        if final_path[2] == "L":
            left_Z_steps = final_path[3]
            assert left_Z_steps != 0
            for _Z_step in range(left_Z_steps):
                self.apply_Z((x + 1) % L, y)
                x, y = (x + 1) % L, (y - 1) % L
        elif final_path[2] == "R":
            right_Z_steps = final_path[3]
            assert right_Z_steps != 0
            for _Z_step in range(right_Z_steps):
                self.apply_Z(x, (y + 1) % L)
                x, y = (x - 1) % L, (y + 1) % L
        else:
            assert final_path[2] == "O"
            assert final_path[3] == 0

        # Ensure that we arrived at the second plaquette defect.
        assert (x, y) == (x2, y2)

    def is_in_trivial_state(self):
        """
        Checks for type 1 logical operators. These are rows or columns of
        alternating X and Z operators (XZXZ) or any muplication of these
        by stabilisers.

        :return: Whether it is in trivial state with respect to the logical
        operators or not.
        :rtype: Bool
        """
        L = self.size
        # Check parity along rows
        for i in range(L):
            total = 0
            for j in range(L):
                if self.qubits[i][j].state == 'Z' or self.qubits[i][j].state == 'X':
                    total += 1
                if self.qubits[i][j].state == 'Y':
                    total += 2
            if total % 2 == 1:
                return False
        # Check parity along columns
        for j in range(L):
            total = 0
            for i in range(L):
                if self.qubits[i][j].state == 'Z' or self.qubits[i][j].state == 'X':
                    total += 1
                if self.qubits[i][j].state == 'Y':
                    total += 2
            if total % 2 == 1:
                return False
        return True

    def diagonals_to_list(self):
        """
        Returns the Z symmetry diagonals of the lattice.

        Notes:

        * This is used as a helper function for :meth:`is_in_trivial_state_Z`

        :return: Z symmetry diagonals as a list of coordinates. [diagonal1,
        diagonal2, ...] where diagonal1 = [(x1,y1),(x2,y2),...] are the
        coordinates which make up the diagonal.
        :rtype: List of lists of tuples of int
        """
        L = self.size
        diagonals = []
        for i in range(L):
            diagonal = []
            for j in range(L):
                diagonal.append(((i - j) % L, j))
            diagonals.append(diagonal)
        return diagonals

    def is_in_trivial_state_Z(self):
        """
        Checks for type 2 logical operators. These are rows or columns of Y
        operators (YYYY) or any muplication of these by stabilisers. It also
        detects type 1 logical operators.

        :return: Whether it is in trivial state with respect to the logical
        operators or not.
        :rtype: Bool
        """
        diagonals = self.diagonals_to_list()
        for d in diagonals:
            total = 0
            for i, j in d:
                if self.qubits[i][j].state == 'X' or self.qubits[i][j].state == 'Y':
                    total += 1
            if total % 2 == 1:
                return False
        return True

    def is_in_code_space(self):
        """
        Checks whether the state of the lattice is in the code space and
        therefore suitable for making an inference about its logical error
        state.

        :return: Whether it is in the code space or not.
        :rtype: Bool
        """
        L = self.size
        for i in range(L):
            for j in range(L):
                if self.plaquettes[i][j].state == 1:
                    return False
        return True

    def __repr__(self):
        strn = []
        for i in range(self.size):
            for j in range(self.size):
                strn.append(self.qubits[i][j].state)
            strn.append('\n')
        print(strn)
        return "".join(strn)
