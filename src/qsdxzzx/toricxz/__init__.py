"""
This module contains implementations relevant to XZZX toric stabilizer codes and minimum-weight-matching decoding.

**Note**: The scripts in this module are not written as qecsim extensions and therefore do not integrate into the qecsim
command line. Details on running simulations with this module can be found in the README and data/README files of the
repository: https://bitbucket.org/qecsim/qsdxzzx/
"""
