import os
import time
import pickle
import sys

import CoprimeLattice as cl
import generate_paths_coprime_lattice as gp
import qecsim.graphtools as gt


def decode_inf_Z_bias(lattice):
    """
    Decode pure dephasing noise in the coprime periodic lattice by matching
    along the symmetry diagonal.

    Note:

    * This is analogous to decoding a single repetition code.

    :param lattice: The coprime periodic lattice with pure dephasing noise on
    some of its qubits.
    :type lattice: object
    """
    L = lattice.size
    G = gt.SimpleGraph()
    Z_diagonal = lattice.get_Z_diagonal()

    nodes = []
    for i, j in Z_diagonal:
        if lattice.plaquettes[i][j].state == 1:
            nodes.append(str(i) + ',' + str(j))
    # Match along the diagonal.
    matching = []
    for v1 in nodes:
        for v2 in nodes:
            if v1 != v2:
                x1, y1 = int(v1.split(",")[0]), int(v1.split(",")[1])
                x2, y2 = int(v2.split(",")[0]), int(v2.split(",")[1])
                left_dis = (Z_diagonal.index((x1, y1)) - Z_diagonal.index((x2, y2))) % (L * (L + 1))
                right_dis = (Z_diagonal.index((x2, y2)) - Z_diagonal.index((x1, y1))) % (L * (L + 1))
                dis = min([left_dis, right_dis])
                G.add_edge(v1, v2, dis)
    matching += list(gt.mwpm(G))

    for u, v in matching:
        lattice.correct_inf_Z(u, v)


def decode_high_rate_Z(lattice, p, bias, paths_dic):
    """
    Decode biased noise towards dephasing.

    :param lattice: The coprime periodic lattice with biased dephasing noise on
    its qubits.
    :type lattice: object
    :param p: Proability of an error on any one qubit.
    :type p: float
    :param bias: Bias coeffiecient.
    :type bias: float
    :param paths_dic: Dictionary of pre-determined weights between any two
    defects in the lattice. Keys of the form (coord1,coord2) where coord=(x,y).
    Value of the form: weight.
    :type paths_dic: dict
    """
    L = lattice.size

    Z_diagonal = lattice.get_Z_diagonal()  # All coords in lattice.
    nodes = []
    for u in Z_diagonal:
        if lattice.plaquettes[u[0]][u[1]].state == 1:
            nodes.append(u)
    G = gt.SimpleGraph()
    for v1 in nodes:
        for v2 in nodes:
            if v1 != v2:
                x1, y1, x2, y2 = v1[0], v1[1], v2[0], v2[1]
                path = paths_dic[((x2 - x1) % L, (y2 - y1) % (L + 1))]
                G.add_edge(str(x1) + "," + str(y1), str(x2) + "," + str(y2), path[4])
    matching = list(gt.mwpm(G))

    for u, v in matching:
        x1, y1, x2, y2 = int(u.split(",")[0]), int(u.split(",")[1]), int(v.split(",")[0]), int(v.split(",")[1])
        path_uv = paths_dic[((x2 - x1) % L, (y2 - y1) % (L + 1))]
        lattice.correct_given_path(u, v, path_uv)

##############################################################################


def test(p, bias, size, N, keep_paths_dic=False):
    """
    Run N simulations of error input and error correction on a coprime lattice.

    :param p: Probability of an error on any one physical qubit.
    :type p: float
    :param bias: Bias coefficient. If bias equals 'inf' then noise is
    infinitely biased, otherwise bias is a float greater than 0 quantifying the
    bias toward dephasing.
    :type bias: str or float
    :param size: Size L of Lx(L+1) lattice. Must be an odd number.
    :type size: int
    :param N: Number of simulations to run.
    :type N: int
    :param keep_paths_dic: Whether to keep the created path dictionary in the
    current folder or delete it.
    :type keep_paths_dic: boolean
    """
    start_time = time.time()  # Time simulations.

    assert size % 2 == 1

    if bias != 'inf':
        # Load or create path file
        filename = "coprime_distance_table_" + str(size) + "_" + str(bias) + "_" + str(p) + ".pkl"
        if not os.path.isfile(filename):
            gp.generate_path(size, bias, p)

        pkl_file = open("coprime_distance_table_" + str(size) + "_" + str(bias) + "_" + str(p) + ".pkl", 'rb')
        paths_dic = pickle.load(pkl_file)
        pkl_file.close()

    total_fail = 0
    for _ in range(N):
        lattice = cl.CoprimeLattice(size)

        if bias != 'inf':
            lattice.add_high_rate_Z_noise(p, bias)
            decode_high_rate_Z(lattice, p, bias, paths_dic)
        else:
            lattice.add_inf_noise('Z', p)
            decode_inf_Z_bias(lattice)

        if not lattice.is_in_code_space():
            assert True is False
        if (not lattice.is_in_trivial_state_1()) or (not lattice.is_in_trivial_state_2()):
            total_fail += 1

    print([{"code: Rotated coprime XZ " + str(L) + "x" + str(L + 1),
            "decoder: Rotated coprime Lx(L+1) XZ MWPM",
            "error_model: Biased noise toward dephasing",
            "bias: " + str(bias),
            "error_probability: " + str(p),
            "logical_failure_rate: " + str(total_fail / N),
            "measurement_error_probability: " + str(0),
            "n_run: " + str(N),
            "n_fail: " + str(total_fail),
            "wall_time: " + str(time.time() - start_time)}])

    if bias != 'inf':
        if not keep_paths_dic:
            os.remove("coprime_distance_table_" + str(size) + "_" + str(bias) + "_" + str(p) + ".pkl")


if __name__ == "__main__":

    try:
        assert len(sys.argv) >= 5
        all_arguments = []
        for i in range(len(sys.argv)):
            if i > 0:
                argument = sys.argv[i]
                key, value = argument.split('=')
                if key == "p":
                    all_arguments.append("p")
                    p = float(value)
                elif key == "bias":
                    all_arguments.append("bias")
                    if value == 'inf':
                        bias = 'inf'
                    else:
                        bias = float(value)
                elif key == "L":
                    all_arguments.append("L")
                    L = int(value)
                    assert L % 2 == 1
                elif key == "N":
                    all_arguments.append("N")
                    N = int(value)
                else:
                    assert key == "keep_paths_dic"
                    keep_paths_dic = bool(value)
        assert all(item in all_arguments for item in ["p", "bias", "L", "N"])

    except Exception:
        print("#######################")
        print()
        print("Incorrect input syntax.")
        print(".......................")
        print("Run this program as: ")
        print()
        print("python coprime_lattice_mwpm_decoder.py p=<physical_error_rate>"
              + " bias=<bias_coefficient> L=<size_of_coprime_Lx(L+1)_lattice>"
              + " N=<tot_number_of_simulations>")
        print()
        print("<physical_error_rate>: float between 0 and 1 (inclusive)")
        print("<bias_coefficient>: float greater than 0. Can also be str"
              + " 'inf' to use a noise model infinitely biased toward dephasing")
        print("<size_of_coprime_Lx(L+1)_lattice>: int positive and odd.")
        print("<tot_number_of_simulations>: int greater than zero.")
        print(".......................")
        print("Optional arguments:")
        print("keep_paths_dic=<Boolean (True or False)>: Whether to keep "
              + "the look-up tables used to assign the weight between two "
              + "defects for the MWPM step or not. The default is False.")
        print(".......................")
        print("Example: ")
        print("python coprime_lattice_mwpm_decoder.py p=0.2 bias=300 L=7 N=1000")
        print()
        print("#######################")

        sys.exit()

    try:
        keep_paths_dic
    except NameError:
        keep_paths_dic = False

    test(p, bias, L, N, keep_paths_dic)
