import math
import pickle

import SquareLattice as sl


def diagonals_to_list(L):
    """
    Retrieves the Z symmetry diagonals of a square lattice. These are the ones
    that go like this: //

    :param L: Size of LxL lattice.
    :type L: int
    :return: List of diagonals in the form: [diagonal1, diagonal2, ...] where
    each diagonal has the form diagonal=[coord1, coord2, ...] and each coord
    has the form (x,y).
    :rtype: List of lists of tuples of int
    """
    diagonals = []
    for i in range(L):
        diagonal = []
        for j in range(L):
            diagonal.append(((i - j) % L, j))
        diagonals.append(diagonal)
    return diagonals


def other_diagonals_to_list(L):
    """
    Retrieves the X symmetry diagonals of a square lattice. These are the ones
    that go like this: \\

    :param L: Size of LxL lattice.
    :type L: int
    :return: List of diagonals in the form: [diagonal1, diagonal2, ...] where
    each diagonal has the form diagonal=[coord1, coord2, ...] and each coord
    has the form (x,y).
    :rtype: List of lists of tuples of int
    """
    diagonals = []
    for i in range(L):
        diagonal = []
        for j in range(L):
            diagonal.append(((i + j) % L, j))
        diagonals.append(diagonal)
    return diagonals


def diagonal_containing(u, L):
    """
    Retrieves the Z symmetry diagonal (//) of a square lattice which contains
    the vertex u.

    :param u: Coordinate which specifies which diagonal to select. In the form
    u=(x,y).
    :type u: tuple of int
    :param L: Size of LxL lattice.
    :type L: int
    :return: Diagonal which contains u, in the form: diagonal=[coord1,
    coord2, ...] where each coord has the form (x,y).
    :rtype: List of tuples of int
    """
    diagonals = diagonals_to_list(L)
    for d in diagonals:
        if u in d:
            return d


def other_diagonal_containing(u, L):
    """
    Retrieves the X symmetry diagonal (\\) of a square lattice which contains
    the vertex u.

    :param u: Coordinate which specifies which diagonal to select. In the form
    u=(x,y).
    :type u: tuple of int
    :param L: Size of LxL lattice.
    :type L: int
    :return: Diagonal which contains u, in the form: diagonal=[coord1,
    coord2, ...] where each coord has the form (x,y).
    :rtype: List of tuples of int
    """
    diagonals = other_diagonals_to_list(L)
    for d in diagonals:
        if u in d:
            return d


def get_path(lattice, u, v, high_weight, low_weight):
    """
    Returns the weight of the path between 2 defects and the path. Matching
    along the Z symmetry diagonals (//) is favoured and matching along the
    X symmetry diagonals (\\) is penalised.

    :param lattice: The square periodic lattice with biased dephasing noise on
    its qubits.
    :type lattice: object
    :param u: Coordinate of first defect in the form u=(x,y)
    :type u: tuple of int
    :param v: Coordinate of first defect in the form v=(x,y)
    :type v: tuple of int
    :param high_weight: Weight assigned to a step along the X symmetry
    diagonals.
    :type high_weight: float
    :param low_weight: Weight assigned to a step along the Z symmetry
    diagonals.
    :type low_weight: float
    :return: Path and weight of path in the form: final_path = [initial X move,
    number of X moves, initial Z move, number of Z moves, weight] where initial
    X move and initial Z move = "L","R" or "O".
    :rtype: list of str, int, str, int, float
    """
    L = lattice.size
    x1, y1, x2, y2 = u[0], u[1], v[0], v[1]

    # Cannot match if defects are not in plaquettes of the same parity (in
    # terms of indices)
    if (x1 + y1) % 2 != (x2 + y2) % 2:
        return None

    d1 = diagonal_containing((x1, y1), L)  # This one: //
    d2 = other_diagonal_containing((x1, y1), L)  # This one: \\

    final_path = [None, None, None, None, high_weight * L * L * L]

    # Consider going from u straight to v along the low weight diagonals,
    # if possible.
    if (x2, y2) in d1:
        right_moves, left_moves = (y2 - y1) % L, (y1 - y2) % L
        if left_moves <= right_moves:
            weight = left_moves * low_weight
            if weight < final_path[4]:
                final_path = ["O", 0, "L", left_moves, weight]
        else:
            weight = right_moves * low_weight
            if weight < final_path[4]:
                final_path = ["O", 0, "R", right_moves, weight]

    # Consider going from u straight to v along the high weight diagonals,
    # if possible.
    if (x2, y2) in d2:
        right_moves, left_moves = (y2 - y1) % L, (y1 - y2) % L
        if left_moves <= right_moves:
            weight = left_moves * high_weight
            if weight < final_path[4]:
                final_path = ["L", left_moves, "O", 0, weight]
        else:
            weight = right_moves * high_weight
            if weight < final_path[4]:
                final_path = ["R", right_moves, "O", 0, weight]

    d3 = diagonal_containing((x2, y2), L)

    # Start at u=(x1,y1). Move RIGHT along \\ and stop when we get to //
    # containing v=(x2,y2). Then count how many steps along these // to
    # get to (x2,y2):
    right_X_moves = 0
    for j in range(L):
        right_X_moves += 1
        if ((x1 + 1 + j) % L, (y1 + 1 + j) % L) in d3:
            _, y_end = ((x1 + 1 + j) % L, (y1 + 1 + j) % L)
            break
    right_Z_moves, left_Z_moves = (y2 - y_end) % L, (y_end - y2) % L
    if left_Z_moves <= right_Z_moves:
        weight = right_X_moves * high_weight + left_Z_moves * low_weight
        if weight < final_path[4]:
            final_path = ["R", right_X_moves, "L", left_Z_moves, weight]
    else:
        weight = right_X_moves * high_weight + right_Z_moves * low_weight
        if weight < final_path[4]:
            final_path = ["R", right_X_moves, "R", right_Z_moves, weight]

    # Start at u=(x1,y1). Move LEFT along \\ and stop when we get to //
    # containing v=(x2,y2). Then count how many steps along these // to
    # get to (x2,y2):
    left_X_moves = 0
    for j in range(L):
        left_X_moves += 1
        if ((x1 - 1 - j) % L, (y1 - 1 - j) % L) in d3:
            _, y_end = ((x1 - 1 - j) % L, (y1 - 1 - j) % L)
            break
    right_Z_moves, left_Z_moves = (y2 - y_end) % L, (y_end - y2) % L
    if left_Z_moves <= right_Z_moves:
        weight = left_X_moves * high_weight + left_Z_moves * low_weight
        if weight < final_path[4]:
            final_path = ["L", left_X_moves, "L", left_Z_moves, weight]
    else:
        weight = left_X_moves * high_weight + right_Z_moves * low_weight
        if weight < final_path[4]:
            final_path = ["L", left_X_moves, "R", right_Z_moves, weight]

    return final_path


def generate_path(L, bias, p):
    """
    Generates table for relative paths on an LxL lattice.

    :param L: Size of LxL lattice.
    :type L: int
    :param bias: Bias coeffiecient.
    :type bias: float
    :param p: Proability of an error on any one qubit.
    :type p: float
    """
    p_h = (bias * p) / (1 + bias)
    p_l = (p) / (2 * (1 + bias))
    high_weight = int(-math.log(p_l / (1 - p)) * 1000000)
    low_weight = int(-math.log(p_h / (1 - p)) * 1000000)

    lattice = sl.SquareLattice(L)

    # Create a path dictionary. Keys take the form (coord1,coord2) where each
    # coord=(x,y) is a coordinate of a defect. The key is a path list, the
    # output from the function `get_path`. Only calculate relative paths to
    # (0,0). It's a simple translation to get the path between any 2 arbitrary
    # defects.
    paths_dic = {}

    u = (0, 0)
    for i in range(L):
        for j in range(L):
            v = (i, j)
            if u != v:
                path = get_path(lattice, u, v, high_weight, low_weight)
                paths_dic[v] = path

    # Save the dictionary into the current folder.
    with open("L_L_distance_table_" + str(L) + "_" + str(bias) + "_" + str(p) + ".pkl", 'wb') as f:
        pickle.dump(paths_dic, f)
