import os
import sys
import pickle
import random
import math
import time

import numpy as np

import CoprimeLattice as cl
import coprime_lattice_mwpm_decoder as decoder
import generate_paths_coprime_lattice as gp


"""
This module contains the code required to conduct sampling of error
configurations from the XZZX code under a biased noise model toward
dephasing. The notation of the functions follows that used by the article
'The role of entropy in topological quantum error correction'
(arXiv:1812.05117) Appendix D as well as the original article where the
method was conceived: 'Simulation of rare events in quantum error correction'
(PhysRevA.88.062308).

This code also utilises the sampled configurations together with the splitting
method (PhysRevA.88.062308) to calculate logical error probabilities at very
low physical error proabilities where Monte carlo simulations would be
untractable.
"""

"""
In the following functions we define the XZZX lattice on a dictionary
structure as well the action of Pauli errors on its qubits.
"""


def create_dic_lattice(d):
    """
    Creates the XZZX coprime periodic lattice as a dictionary structure.

    :param d: Size of dx(d+1) lattice.
    :type d: int
    :return: Lattice as a dictionary where keys are the coordinates of the
    physical qubits in the form (x,y) and values are the Pauli state of the
    qubits.
    :rtype: dict
    """
    dic = {}
    for i in range(d):
        for j in range(d + 1):
            dic[(i, j)] = 'I'
    return dic


def print_dic_lattice(dic, d):
    """
    Prints the dictionary lattice as an array to terminal.

    :param dic: Dictionary lattice.
    :type dic: dict
    :param d: Size of dx(d+1) lattice.
    :type d: int
    """
    for i in range(d):
        print()
        for j in range(d + 1):
            print(dic[(i, j)], end="")


def get_error_type(p_h, p_l, p):
    """
    Given a probability distribution and assuming that an error occurred on a
    qubit, it outputs the type of Pauli error to be applied to the qubit
    (either X, Y or Z).

    :param p_h: Probability of a high rate error (i.e. of Z).
    :type p_h: float
    :param p_l: Probability of a low rate error (i.e of X or Y).
    :type p_l: float
    :param p: Probability of an error on any one physical qubit.
    :type p: float
    :return: The type of Pauli error to be applied to the qubit in string
    form, either 'X', 'Y' or 'Z'.
    :rtype: str
    """
    normaliser = 1 / p
    py_n, px_n, pz_n = p_l * normaliser, p_l * normaliser, p_h * normaliser
    r = random.random()
    if r <= pz_n:
        return 'Z'
    elif pz_n < r <= (pz_n + px_n):
        return 'X'
    elif (px_n + pz_n) < r <= (pz_n + px_n + py_n):
        return 'Y'


def apply_state1_to_state2(state1, state2):
    """
    Models applying one Pauli qubit state to another Pauli qubit state.
    i.e. it calculates the multiplication of Pauli operators.

    :param state1: Pauli state to be applied, in string form. Either 'X',
    'Y' or 'Z'.
    :type state1: str
    :param state2: Pauli state to which state1 is applied, in string form.
    Either 'X', 'Y', 'Z' or 'I' (in trivial state).
    :type state2: str
    :return: Pauli state of qubit after state1 is applied to state2. Either
    'X', 'Y', 'Z' or 'I' (trivial state).
    :rtype: str
    """
    assert state1 in ['X', 'Y', 'Z']
    if (state1 == 'X' and state2 == 'I') or (state1 == 'Y' and state2 == 'Z') or (state1 == 'Z' and state2 == 'Y'):
        return 'X'
    elif (state1 == 'Y' and state2 == 'I') or (state1 == 'X' and state2 == 'Z') or (state1 == 'Z' and state2 == 'X'):
        return 'Y'
    elif (state1 == 'Z' and state2 == 'I') or (state1 == 'Y' and state2 == 'X') or (state1 == 'X' and state2 == 'Y'):
        return 'Z'
    elif (state1 == 'X' and state2 == 'X') or (state1 == 'Y' and state2 == 'Y') or (state1 == 'Z' and state2 == 'Z'):
        return 'I'


def load_paths_dic(p, bias, size, keep_paths_dic=False):
    """
    Loads a path dictionary containing the paths between plaquette defects
    as well as the weight of the path. If the dictionary file doesn't exist
    then it creates one and then loads it in.

    :param p: Probability of an error on any one physical qubit.
    :type p: float
    :param bias: Bias coefficient. It is a number greater than 0 quantifying
    the bias toward dephasing.
    :type bias: float
    :param size: Size L of Lx(L+1) lattice.
    :type size: int
    :param keep_paths_dic: Whether to keep the created path dictionary in the
    current folder or delete it.
    :type keep_paths_dic: boolean
    :return: The paths dictionary with keys a tuple of the coordinates of the
    two defects to be matched and with value equal to a list containing the
    matching between the defects and the weight of the matched path.
    :rtype: dic
    """
    # Load or create path file.
    filename = "coprime_distance_table_" + str(size) + "_" + str(bias) + "_" + str(p) + ".pkl"
    if not os.path.isfile(filename):
        gp.generate_path(size, bias, p)
    pkl_file = open("coprime_distance_table_" + str(size) + "_" + str(bias) + "_" + str(p) + ".pkl", 'rb')
    paths_dic = pickle.load(pkl_file)
    pkl_file.close()

    if not keep_paths_dic:
        os.remove("coprime_distance_table_" + str(size) + "_" + str(bias) + "_" + str(p) + ".pkl")
    return paths_dic


"""
In the following functions we define the tools required to conduct the
metropolis sampling algorithm and splitting method of PhysRevA.88.062308.
"""


def g(x):
    """
    Detailed balance function as introduced by PhysRevA.88.062308.

    :param x: Input into the real valued function.
    :type x: float
    :return: Value of the function
    :rtype: float
    """
    return 1 / (1 + x)


def prob_err(d, p, p_h, p_l, H, L):
    """
    Calculates the probability of observing a particular error configuration
    on the coprime XZZX lattice given the number of high rate and low rate
    errors as well as their probabilities.

    :param d: Size of the dx(d+1) lattice.
    :type d: int
    :param p: Probability of an error on any one physical qubit.
    :type p: float
    :param p_h: Probability of a high rate error (i.e. of Z).
    :type p_h: float
    :param p_l: Probability of a low rate error (i.e of X or Y).
    :type p_l: float
    :param H: Total number of high rate errors.
    :type H: int
    :param L: Total number of low rate errors.
    :type L: int
    :return: Probability of observing error configuration under the biased
    noise model presented in the article 'The XZZX surface code'.
    :rtype: float
    """
    n = d * (d + 1)  # Total number of qubits.
    return (p_h ** (H)) * (p_l ** (L)) * ((1 - p) ** (n - H - L))


def prob_err1_div_prob_err2(d, p, p_h, p_l, H1, H2, L1, L2):
    """
    Calculates the probability of observing a particular error configuration
    on divided by the probability of observing a second error configuration
    on the coprime XZZX lattice given the number of high rate and low rate
    errors for each observed error.

    :param d: Size of the dx(d+1) lattice.
    :type d: int
    :param p: Probability of an error on any one physical qubit.
    :type p: float
    :param p_h: Probability of a high rate error (i.e. of Z).
    :type p_h: float
    :param p_l: Probability of a low rate error (i.e of X or Y).
    :type p_l: float
    :param H1: Total number of high rate errors in first configuration.
    :type H1: int
    :param L1: Total number of low rate errors in first configuration.
    :type L1: int
    :param H2: Total number of high rate errors in second configuration.
    :type H2: int
    :param L2: Total number of low rate errors in second configuration.
    :type L2: int
    :return: Probability of observing error configuration 1 divided by
    probability of observing error configuration 2 under the biased
    noise model presented in the article 'The XZZX surface code'.
    :rtype: float
    """
    return (p_h ** (H1 - H2)) * (p_l ** (L1 - L2)) * ((1 - p) ** (H2 + L2 - H1 - L1))


def A_j(p1_h, p1_l, p1, p2_h, p2_l, p2, d, H, L):
    """
    Probability of an error configuration with H high rate errors and L
    low rate errors under the distribution (p2, p2_h, p2_l) divided by
    the probability distribution of the same error configuration under the
    distribution (p1, p1_h, p1_l).

    :param p1_h: Probability of a high rate error (i.e. of Z) under the first
    distribution.
    :type p1_h: float
    :param p1_l: Probability of a low rate error (i.e of X or Y) under the
    first distribution.
    :type p1_l: float
    :param p1: Probability of an error on any one physical qubit under the
    first distribution.
    :type p1: float
    :param p2_h: Probability of a high rate error (i.e. of Z) under the second
    distribution.
    :type p2_h: float
    :param p2_l: Probability of a low rate error (i.e of X or Y) under the
    second distribution.
    :type p2_l: float
    :param p2: Probability of an error on any one physical qubit under the
    second distribution.
    :type p2: float
    :param d: Size of the dx(d+1) lattice.
    :type d: int
    :param H: Total number of high rate errors in the error configuration.
    :type H: int
    :param L: Total number of low rate errors in the error configuration.
    :type L: int
    :return: Probability of error under first distribution over probability
    of error under the second distribution.
    :rtype: float
    """
    assert p2 > p1
    return prob_err(d, p2, p2_h, p2_l, H, L) / prob_err(d, p1, p1_h, p1_l, H, L)


def expec_G_CAj(p1_h, p1_l, p1, p2_h, p2_l, p2, d, C, E):
    """
    Calculates the expectation value of the constant C times Aj, as defined in
    :func:`A_j`, numerically given an array of error configurations (see
    PhysRevA.88.062308 equation (8)).

    :param p1_h: Probability of a high rate error (i.e. of Z) under the first
    distribution.
    :type p1_h: float
    :param p1_l: Probability of a low rate error (i.e of X or Y) under the
    first distribution.
    :type p1_l: float
    :param p1: Probability of an error on any one physical qubit under the
    first distribution.
    :type p1: float
    :param p2_h: Probability of a high rate error (i.e. of Z) under the second
    distribution.
    :type p2_h: float
    :param p2_l: Probability of a low rate error (i.e of X or Y) under the
    second distribution.
    :type p2_l: float
    :param p2: Probability of an error on any one physical qubit under the
    second distribution.
    :type p2: float
    :param d: Size of the dx(d+1) lattice.
    :type d: int
    :param C: Constant as defined in PhysRevA.88.062308 equation (6).
    :type C: float
    :param E: Dictionary of error configurations in the form
    E = {(H1, L1): n1, ...., (Hk, Lk): nk} where each entry takes the form
    (# of high rate errs, # of low rate errs) : # of times such configuration
    appears.
    :type E: dict
    :return: Un-normalised expectation value of C times Aj calculated using
    the provided error configurations in E.
    :rtype: float
    """
    assert p1 < p2
    total = 0
    for w, n in E.items():
        H, L = w
        total += n * g(C * A_j(p1_h, p1_l, p1, p2_h, p2_l, p2, d, H, L))
    return total


def expec_G_CAj1(p1_h, p1_l, p1, p2_h, p2_l, p2, d, C, E):
    """
    Calculates the expectation value of one over the constant C times Aj,
    as defined in :func:`A_j`, numerically given an array of error
    configurations (see PhysRevA.88.062308 equation (8)).

    :param p1_h: Probability of a high rate error (i.e. of Z) under the first
    distribution.
    :type p1_h: float
    :param p1_l: Probability of a low rate error (i.e of X or Y) under the
    first distribution.
    :type p1_l: float
    :param p1: Probability of an error on any one physical qubit under the
    first distribution.
    :type p1: float
    :param p2_h: Probability of a high rate error (i.e. of Z) under the second
    distribution.
    :type p2_h: float
    :param p2_l: Probability of a low rate error (i.e of X or Y) under the
    second distribution.
    :type p2_l: float
    :param p2: Probability of an error on any one physical qubit under the
    second distribution.
    :type p2: float
    :param d: Size of the dx(d+1) lattice.
    :type d: int
    :param C: Constant as defined in PhysRevA.88.062308 equation (6).
    :type C: float
    :param E: Dictionary of error configurations in the form
    E = {(H1, L1): n1, ...., (Hk, Lk): nk} where each entry takes the form
    (# of high rate errs, # of low rate errs) : # of times such configuration
    appears.
    :type E: dict
    :return: Un-normalised expectation value of 1 / (C times Aj) calculated
    using the provided error configurations in E.
    :rtype: float
    """
    assert p1 < p2
    total = 0
    for w, n in E.items():
        H, L = w
        total += n * g(1 / (C * A_j(p1_h, p1_l, p1, p2_h, p2_l, p2, d, H, L)))
    return total


"""
Below are functions for possible different initialisation states of the error
configuration when conducting the metropolis sampling algorithm.
"""


def initialisation1(d):
    """
    Initialises the lattice with the logical error consisting of a Z operator
    on all of its qubits.

    :param d: Size of dx(d+1) lattice.
    :type d: int
    :return: Dictionary lattice with a Z operator on every qubit.
    :rtype: dict
    """
    current_lattice = create_dic_lattice(d)
    for i in range(d):
        for j in range(d + 1):
            current_lattice[(i, j)] = 'Z'
    return current_lattice


def initialisation2(d):
    """
    Initialises the lattice with the logical error consisting of a Y operator
    on every qubit on the first column.

    :param d: Size of dx(d+1) lattice.
    :type d: int
    :return: Dictionary lattice with a column of Y operators.
    :rtype: dict
    """
    current_lattice = create_dic_lattice(d)
    for i in range(d):
        current_lattice[(i, 0)] = 'Y'
    return current_lattice


def initialisation3(d):
    """
    Initialises the lattice with the logical error consisting of a row of
    alternating X and Z operators.

    :param d: Size of dx(d+1) lattice.
    :type d: int
    :return: Dictionary lattice with a XZXZ logical operator.
    :rtype: dict
    """
    current_lattice = create_dic_lattice(d)
    for i in range(d + 1):
        if i % 2 == 0:
            current_lattice[(0, i)] = 'Z'
        else:
            current_lattice[(0, i)] = 'X'
    return current_lattice


def sample_E(N, d, p_h, p_l, p, bias, N_cut):
    """
    Sample error configurations using the metropolis hastings algorithm
    presented in PhysRevA.88.062308.

    :param N: Number of metropolis sampling steps.
    :type N: int
    :param d: Size of the dx(d+1) lattice.
    :type d: int
    :param p_h: Probability of a high rate error (i.e. of Z).
    :type p_h: float
    :param p_l: Probability of a low rate error (i.e of X or Y).
    :type p_l: float
    :param p: Probability of an error on any one physical qubit.
    :type p: float
    :param bias: Bias coefficient. It is a number greater than 0 quantifying
    the bias toward dephasing.
    :type bias: float
    :param N_cut: The number of simulations ran before we start to sample
    error configurations.
    :type N_cut: int
    """
    # Initialise in the required error state.
    current_lattice = initialisation3(d)

    # Initialise the dictionary of sampled error configurations in the form:
    # E = {(H1, L1): n1, ...., (Hk, Lk): nk}
    #   = { (# of high rate errs, # of low rate errs) : # of times such an
    #      error configuration appears}
    E = {}

    # Run simulations.
    for counter in range(N):
        # Propose new error configuration by introducing an error onto a qubit.
        proposed_lattice = current_lattice.copy()
        err_indx = int(np.random.uniform(0, d * (d + 1)))  # Sample error qubit
        x_ix, y_ix = int(err_indx / (d + 1)), err_indx % (d + 1)

        errType = get_error_type(p_h, p_l, p)  # Either X, Y or Z.
        currentState = proposed_lattice[(x_ix, y_ix)]
        # Apply Error to lattice
        proposed_lattice[(x_ix, y_ix)] = apply_state1_to_state2(errType, currentState)

        # Total number of high rate and low rate errors in Current lattice
        current_qubits = current_lattice.values()
        currentH = list(current_qubits).count('Z')
        currentL = list(current_qubits).count('X') + list(current_qubits).count('Y')

        # Total number of high rate and low rate errors in Proposed lattice
        proposed_qubits = proposed_lattice.values()
        proposedH = list(proposed_qubits).count('Z')
        proposedL = list(proposed_qubits).count('X') + list(proposed_qubits).count('Y')

        # Metropolis step.
        r = random.random()
        if r <= min([1, prob_err1_div_prob_err2(d, p, p_h, p_l, proposedH, currentH, proposedL, currentL)]):
            # Non-trivial metropolis step. Attemp to correct the new error
            # configuration. If logical error achieved then the proposed error
            # configuration becomes the new error configuration.

            to_decode = cl.CoprimeLattice(d)
            to_decode.add_err_from_dic(proposed_lattice)
            paths_dic = load_paths_dic(p, bias, d)
            decoder.decode_high_rate_Z(to_decode, p, bias, paths_dic)
            assert to_decode.is_in_code_space()
            if (not to_decode.is_in_trivial_state_1()) or (not to_decode.is_in_trivial_state_2()):
                current_lattice = proposed_lattice.copy()
                currentH, currentL = proposedH, proposedL

        # Start sampling the error configuration after sufficiently long when
        # equilibration has been achieved. This varies on a case to case
        # basis. For the simulations in the article 'The XZZX surface code' we
        # found that after 50000 iterations equilibration had been achieved.
        if counter >= N_cut:
            H, L = currentH, currentL
            if (H, L) not in E:
                E[(H, L)] = 1
            else:
                E[(H, L)] += 1
    return E


def R_j_given_E(p1_h, p1_l, p1, p2_h, p2_l, p2, E1, E2, d):
    """
    Given two probability distributions, P1 and P2, and the error
    configurations sampled under each, it calculates the ratio of logical
    failure rate under the first distribution over the logical failure rate
    under the second distribution. We call this ration R_j.

    Note:

    * R_j is the backbone of the splitting method (PhysRevA.88.062308). R_j
    can be calculated as Rj = C <g(C*Aj)>_P1 / <g(1/(C*Aj))>_P2= C top/bottom,
    where <.>_k is the expectation value of . calculated using the error
    configurations derived from the probability distribution characterised by
    k. When top = bottom, then Rj = C is the required value.

    :param p1_h: Probability of a high rate error (i.e. of Z) under the first
    distribution.
    :type p1_h: float
    :param p1_l: Probability of a low rate error (i.e of X or Y) under the
    first distribution.
    :type p1_l: float
    :param p1: Probability of an error on any one physical qubit under the
    first distribution.
    :type p1: float
    :param p2_h: Probability of a high rate error (i.e. of Z) under the second
    distribution.
    :type p2_h: float
    :param p2_l: Probability of a low rate error (i.e of X or Y) under the
    second distribution.
    :type p2_l: float
    :param p2: Probability of an error on any one physical qubit under the
    second distribution.
    :type p2: float
    :param E1: Error configurations sampled under the first distribution.
    :type E1: dict
    :param E2: Error configurations sampled under the second distribution.
    :type E2: dict
    :param d: Size of the dx(d+1) lattice.
    :type d: int
    :return: Rj, the ratio of logical failure under distribution characterised
    by (p_1, p1_h, p1_l) to logical failure under distribution characterised
    by (p_2, p2_h, p2_l).
    :rtype: float
    """
    assert p1 < p2
    y1, y2 = [], []  # Where R_j = C y1/y2

    C_vals = list(np.linspace(0.0001, 1, 100))

    for C in C_vals:
        val1 = expec_G_CAj(p1_h, p1_l, p1, p2_h, p2_l, p2, d, C, E1) / sum(list(E1.values()))
        val2 = expec_G_CAj1(p1_h, p1_l, p1, p2_h, p2_l, p2, d, C, E2) / sum(list(E2.values()))
        y1.append(val1)
        y2.append(val2)

    indx = np.argmin(np.abs(np.array(y1) - np.array(y2)))
    C_star = C_vals[indx]  # This is R_j
    return C_star


def generate_probs(p_start, p_end, d, n):
    """
    Generates an array of physical error probabilities which will be used as
    the pivot points to calculate the ratios of logical failure rate according
    to the splitting method (PhysRevA.88.062308). The sequence of
    probabilities is calculated according to equation (17) in
    PhysRevA.88.062308.

    :param p_start: The small physical error rate where we want to calculate
    the logical error rate at.
    :type p_start: float
    :param p_end: The larger physical error rate where calculating the logical
    error rate is tractable, say, by Monte carlo simulations.
    :type p_end: float
    :param d: Size of dx(d+1) lattice.
    :type d: int
    :param n: Number of physical qubits in the lattice. For the coprime code
    it is d*(d+1).
    :type n: int
    :return: A list of the physical error probabilities to conduct the
    metropolis sampling and splitting method at.
    :rtype: lst of float
    """
    assert p_start < p_end
    probs = []
    p_current = p_start
    probs.append(p_current)
    while True:
        w = max([d / 2, p_current * n])
        p_next = p_current * (2 ** (1 / math.sqrt(w)))
        if p_next > p_end:
            del probs[-1]
            probs.append(p_end)
            break
        probs.append(p_next)
        p_current = p_next
    return probs


def calculate_and_save_samples(p_to_find, bias, d, known_physical_err, N, N_cut):
    """
    Given a physical error rate where we desire to calculate the logical error
    rate at and a physical error rate where we know the logical error rate, it
    partitions the range of probabilities according to eqn. (17) of
    PhysRevA.88.062308, samples error configurations at every partition
    element using the metropolis sampling technique and saves these error
    configurations into files in the working directory.

    :param p_to_find: The small physical error rate where we want to calculate
    the logical error rate at.
    :type p_to_find: float
    :param bias: Bias coefficient
    :type bias: float
    :param d: Size of dx(d+1) lattice.
    :type d: int
    :param known_physical_err: The physical error rate where calculating the
    logical error rate is tractable, say, by Monte carlo simulations.
    :type known_physical_err: float
    :param N: Number of metropolis sampling steps.
    :type N: int
    :param N_cut: The number of simulations ran before we start to sample
    error configurations.
    :type N_cut: int
    """
    probs = generate_probs(p_to_find, known_physical_err, d, d * (d + 1))
    for i in range(len(probs)):
        sampling_p = probs[i]
        p_h, p_l = sampling_p * bias / (1 + bias), sampling_p / (2 * (1 + bias))
        smp_E = sample_E(N, d, p_h, p_l, sampling_p, bias, N_cut)
        f = open("coprime_" + str(d) + "_" + str(p_to_find) + "_" + str(bias) + "_" + str(i) + ".txt", "w")
        for w, n in smp_E.items():
            H, L = w
            f.write(str(H) + " " + str(L) + " " + str(n) + "\n")
        f.close()


def calculate_and_save_single_sample(p_to_find, bias, d, ix_of_sample, known_physical_err, N, N_cut):
    """
    Given a physical error rate where we desire to calculate the logical error
    rate at and a physical error rate where we know the logical error rate, it
    partitions the range of probabilities according to eqn. (17) of
    PhysRevA.88.062308, samples error configurations at a specified partition
    element using the metropolis sampling technique and saves the error
    configurations at the specified physical error rate into files in the
    working directory.

    :param p_to_find: The small physical error rate where we want to calculate
    the logical error rate at.
    :type p_to_find: float
    :param bias: Bias coefficient
    :type bias: float
    :param d: Size of dx(d+1) lattice.
    :type d: int
    :param ix_of_sample: Index of required physical probability where the
    sampling will be conducted on, from the list of the generated partition
    of probabilities.
    :type ix_of_sample: int
    :param known_physical_err: The physical error rate where calculating the
    logical error rate is tractable, say, by Monte carlo simulations.
    :type known_physical_err: float
    :param N: Number of metropolis sampling steps.
    :type N: int
    :param N_cut: The number of simulations ran before we start to sample
    error configurations.
    :type N_cut: int
    """
    probs = generate_probs(p_to_find, known_physical_err, d, d * (d + 1))
    i = ix_of_sample
    sampling_p = probs[i]
    p_h, p_l = sampling_p * bias / (1 + bias), sampling_p / (2 * (1 + bias))
    smp_E = sample_E(N, d, p_h, p_l, sampling_p, bias, N_cut)
    f = open("coprime_" + str(d) + "_" + str(p_to_find) + "_" + str(bias) + "_" + str(i) + ".txt", "w")
    for w, n in smp_E.items():
        H, L = w
        f.write(str(H) + " " + str(L) + " " + str(n) + "\n")
    f.close()


def read_samples_and_calculate_ratio(p_to_find, bias, d, known_physical_err, known_logical_err, N):
    """
    Reads saved error configurations sampled using the Metropolis algorithm
    and uses the splitting method to calculate the logical error rate at the
    physical error rate where a calculation by Monte carlo simulations would
    be intractable.

    :param p_to_find: The small physical error rate where we want to calculate
    the logical error rate at.
    :type p_to_find: float
    :param bias: Bias coefficient
    :type bias: float
    :param d: Size of dx(d+1) lattice.
    :type d: int
    :param known_physical_err: The physical error rate where calculating the
    logical error rate is tractable, say, by Monte carlo simulations.
    :type known_physical_err: float
    :param known_logical_err: The logical error rate at the known physical
    error rate.
    :type known_logical_err: float
    :param N: Number of metropolis sampling steps.
    :type N: int
    :return: The logical error rate at a low physical error rate.
    :rtype: float
    """
    # Generate partition of physical error rates.
    probs = generate_probs(p_to_find, known_physical_err, d, d * (d + 1))

    # Load in saved error configurations.
    sampled_E = []  # List of dictionaries.
    for i in range(len(probs)):
        E_dic = {}
        f = open("coprime_" + str(d) + "_" + str(p_to_find) + "_" + str(bias) + "_" + str(i) + ".txt", "r")
        lines = [line.rstrip().split(" ") for line in f]
        for H, L, n in lines:
            E_dic[(int(H), int(L))] = int(n)
        sampled_E.append(E_dic)

    # Calculate the logical error rate at low physical error rate using the
    # splitting method.
    P = known_logical_err
    for j in range(len(probs) - 1):
        p1, p2 = probs[j], probs[j + 1]
        p1_h, p1_l = p1 * bias / (1 + bias), p1 / (2 * (1 + bias))
        p2_h, p2_l = p2 * bias / (1 + bias), p2 / (2 * (1 + bias))
        P *= R_j_given_E(p1_h, p1_l, p1, p2_h, p2_l, p2, sampled_E[j + 1], sampled_E[j], d)

    return P


def test(p_to_find, bias, d, p_known, P_known, N, N_cut, keep_sampled_errs=False):
    """
    Calculates the logical error rate of the coprime XZZX code under biased
    dephasing noise at low physical error rate, potentially where simulations
    via other methods, such as Monte carlo simulations, are not tractable.

    :param p_to_find: The small physical error rate where we want to calculate
    the logical error rate at.
    :type p_to_find: float
    :param bias: Bias coefficient
    :type bias: float
    :param d: Size of dx(d+1) lattice.
    :type d: int
    :param known_physical_err: The physical error rate where calculating the
    logical error rate is tractable, say, by Monte carlo simulations.
    :type known_physical_err: float
    :param known_logical_err: The logical error rate at the known physical
    error rate.
    :type known_logical_err: float
    :param N: Number of metropolis sampling steps.
    :type N: int
    :param N_cut: The number of simulations ran before we start to sample
    error configurations.
    :type N_cut: int
    :param keep_sampled_errs: Whether to keep the sampled error files in the
    current folder or delete them.
    :type keep_sampled_errs: boolean
    """
    start_time = time.time()
    calculate_and_save_samples(p_to_find, bias, d, p_known, N, N_cut)
    calculated_logical_err = read_samples_and_calculate_ratio(p_to_find, bias, d, p_known, P_known, N)

    print([{"code: Rotated coprime XZ " + str(d) + "x" + str(d + 1),
            "decoder: Rotated coprime XZ MWPM",
            "error_model: Biased noise toward dephasing",
            "bias: " + str(bias),
            "error_probability: " + str(p),
            "logical_failure_rate: " + str(calculated_logical_err),
            "known_p: " + str(p_known),
            "known_P: " + str(P_known),
            "n_Metropolis_runs: " + str(N),
            "n_discarded: " + str(N_cut),
            "wall_time: " + str(time.time() - start_time)}])

    if not keep_sampled_errs:
        probs = generate_probs(p_to_find, p_known, d, d * (d + 1))
        for i in range(len(probs)):
            os.remove("coprime_" + str(d) + "_" + str(p_to_find) + "_" + str(bias) + "_" + str(i) + ".txt")


if __name__ == "__main__":

    try:
        assert len(sys.argv) >= 7
        all_arguments = []
        for i in range(len(sys.argv)):
            if i > 0:
                argument = sys.argv[i]
                key, value = argument.split('=')
                if key == "p":
                    all_arguments.append("p")
                    p = float(value)
                elif key == "bias":
                    all_arguments.append("bias")
                    bias = float(value)
                elif key == "L":
                    all_arguments.append("L")
                    d = int(value)
                elif key == "p_known":
                    all_arguments.append("p_known")
                    p_known = float(value)
                elif key == "P_known":
                    all_arguments.append("P_known")
                    P_known = float(value)
                elif key == "N":
                    all_arguments.append("N")
                    N = int(value)
                elif key == "N_cut":
                    N_cut = int(value)
                else:
                    assert key == "keep_sampled_errs"
                    keep_sampled_errs = bool(value)

        assert all(item in all_arguments for item in ["p", "bias", "L", "p_known", "P_known", "N"])

    except Exception:
        print("#######################")
        print()
        print("Incorrect input syntax.")
        print(".......................")
        print("Run this program as: ")
        print()
        print("python coprime_low_p_sampler.py p=<target_physical_error_rate>"
              + " bias=<bias_coefficient> L=<size_of_coprime_Lx(L+1)_lattice>"
              + " p_known=<physical_error_rate_where_logical_rate_is_known>"
              + " P_known=<logical_rate_at_p_known>"
              + " N=<tot_number_of_metropolis_steps>")
        print()
        print("<target_physical_error_rate>: float between 0 and 1 (inclusive).")
        print("<bias_coefficient>: float greater than 0.")
        print("<size_of_coprime_Lx(L+1)_lattice>: int positive and odd.")
        print("<physical_error_rate_where_logical_rate_is_known>: float between 0 and 1 (inclusive).")
        print("<logical_rate_at_p_known>: float between 0 and 1 (inclusive).")
        print("<tot_number_of_metropolis_steps>: int greater than zero.")
        print(".......................")
        print("Optional arguments:")
        print("N_cut=<int, greater than 0>: The number of simulations ran"
              + " before starting to sample error configurations. The"
              + " default is 0.")
        print("keep_sampled_errs=<Boolean (True or False)>: Whether to keep "
              + "the sampled error configurations for use in the future or "
              + "not. The default is False.")
        print(".......................")
        print("Example: ")
        print("python coprime_low_p_sampler.py p=1e-3 bias=3 L=7 p_known=0.1 P_known=0.02359 N=5000 N_cut=1000")
        print()
        print("#######################")

        sys.exit()

    try:
        N_Cut
    except NameError:
        N_cut = 0

    try:
        keep_sampled_errs
    except NameError:
        keep_sampled_errs = False

    test(p, bias, d, p_known, P_known, N, N_cut, keep_sampled_errs)
