import math
import pickle

import CoprimeLattice as cl


def get_path(lattice, u, v, high_weight, low_weight):
    """
    Returns the weight of the path between 2 defects and the path. Matching
    along the Z symmetry diagonal (//) is favoured and matching along the
    X symmetry diagonal (\\) is penalised.

    :param lattice: The coprime Lx(L+1) periodic lattice with biased dephasing
    noise on its qubits.
    :type lattice: object
    :param u: Coordinate of first defect in the form u=(x,y)
    :type u: tuple of int
    :param v: Coordinate of first defect in the form v=(x,y)
    :type v: tuple of int
    :param high_weight: Weight assigned to a step along the X symmetry
    diagonals.
    :type high_weight: float
    :param low_weight: Weight assigned to a step along the Z symmetry
    diagonals.
    :type low_weight: float
    :return: Path and weight of path in the form: final_path = [initial X move,
    number of X moves, initial Z move, number of Z moves, weight] where initial
    X move and initial Z move = "L","R" or "O".
    :rtype: list of str, int, str, int, float
    """
    L = lattice.size
    x1, y1, x2, y2 = u[0], u[1], v[0], v[1]

    Z_diagonal = lattice.get_Z_diagonal()

    # Consider going from u straight to v along the low weight diagonals.
    right_moves = (Z_diagonal.index((x2, y2)) - Z_diagonal.index((x1, y1))) % (L * (L + 1))
    left_moves = (Z_diagonal.index((x1, y1)) - Z_diagonal.index((x2, y2))) % (L * (L + 1))

    # final path variable is: final_path = [initial X move, number of X moves,
    # initial Z move, number of Z moves, weight] where initial X move and
    # initial Z move = "L","R" or "O".
    final_path = []

    if left_moves <= right_moves:
        final_path = ["O", 0, "L", left_moves, left_moves * low_weight]
    else:
        final_path = ["O", 0, "R", right_moves, right_moves * low_weight]

    # Now move left along the high weight diagonal L + 1 steps.
    x, y = x1, y1  # Initially at u.

    # L+1 total moves. Do (1,L+1+1) for wrapping around once.
    # Do (1,L*(L+1)+1) for all possibilities.
    for left_X_moves in range(1, L + 2):
        # Move left along high weight diagonal.
        x, y = (x - 1) % L, (y - 1) % (L + 1)
        if (x, y) == (x2, y2):  # Check if we arrived at v.
            weight = left_X_moves * high_weight
            if weight < final_path[4]:
                final_path = ["L", left_X_moves, "O", 0, weight]
        else:
            # Now move to v along low weight diagonals.
            right_Z_moves = (Z_diagonal.index((x2, y2)) - Z_diagonal.index((x, y))) % (L * (L + 1))
            left_Z_moves = (Z_diagonal.index((x, y)) - Z_diagonal.index((x2, y2))) % (L * (L + 1))
            if left_Z_moves <= right_Z_moves:
                weight = left_X_moves * high_weight + left_Z_moves * low_weight
                if weight < final_path[4]:
                    final_path = ["L", left_X_moves, "L", left_Z_moves, weight]
            else:
                weight = left_X_moves * high_weight + right_Z_moves * low_weight
                if weight < final_path[4]:
                    final_path = ["L", left_X_moves, "R", right_Z_moves, weight]

    # Now move right along the high weight diagonal L + 1 steps.
    x, y = x1, y1  # Initially at u.
    # L+1 total moves. Do (1,L+1+1) for wrapping around once.
    # Do (1,L*(L+1)+1) for all possibilities.
    for right_X_moves in range(1, L + 2):
        x, y = (x + 1) % L, (y + 1) % (L + 1)  # Move left along high weight diagonal.
        if (x, y) == (x2, y2):  # Check if we arrived at v.
            weight = right_X_moves * high_weight
            if weight < final_path[4]:
                final_path = ["R", right_X_moves, "O", 0, weight]
        else:
            # Now move to v along low weight diagonals.
            right_Z_moves = (Z_diagonal.index((x2, y2)) - Z_diagonal.index((x, y))) % (L * (L + 1))
            left_Z_moves = (Z_diagonal.index((x, y)) - Z_diagonal.index((x2, y2))) % (L * (L + 1))
            if left_Z_moves <= right_Z_moves:
                weight = right_X_moves * high_weight + left_Z_moves * low_weight
                if weight < final_path[4]:
                    final_path = ["R", right_X_moves, "L", left_Z_moves, weight]
            else:
                weight = right_X_moves * high_weight + right_Z_moves * low_weight
                if weight < final_path[4]:
                    final_path = ["R", right_X_moves, "R", right_Z_moves, weight]
    return final_path


def generate_path(L, bias, p):
    """
    Generates table for relative paths on an Lx(L+1) lattice.

    :param L: Size of Lx(L+1) lattice.
    :type L: int
    :param bias: Bias coeffiecient.
    :type bias: float
    :param p: Proability of an error on any one qubit.
    :type p: float
    """
    p_h = (bias * p) / (1 + bias)
    p_l = (p) / (2 * (1 + bias))
    high_weight = int(-math.log(p_l / (1 - p)) * 1000000)
    low_weight = int(-math.log(p_h / (1 - p)) * 1000000)

    lattice = cl.CoprimeLattice(L)
    Z_diagonal = lattice.get_Z_diagonal()  # These are all the positions.

    # Create a path dictionary. Keys take the form (coord1,coord2) where each
    # coord=(x,y) is a coordinate of a defect. The key is a path list, the
    # output from the function `get_path`. Only calculate relative paths to
    # (0,0). It's a simple translation to get the path between any 2 arbitrary
    # defects.
    paths_dic = {}

    u = (0, 0)
    for v in Z_diagonal:
        if u != v:
            path = get_path(lattice, u, v, high_weight, low_weight)
            paths_dic[v] = path
    with open("coprime_distance_table_" + str(L) + "_" + str(bias) + "_" + str(p) + ".pkl", 'wb') as f:
        pickle.dump(paths_dic, f)
